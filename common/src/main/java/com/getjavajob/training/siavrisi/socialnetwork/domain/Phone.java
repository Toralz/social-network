package com.getjavajob.training.siavrisi.socialnetwork.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "phone")
public class Phone implements Comparable<Phone> {

    private static final Logger logger = LoggerFactory.getLogger(Phone.class);

    @Id
    private String phone;

    public Phone(String phone) {
        this.phone = phone;
    }

    public Phone() {

    }

    public String getPhone() {
        logger.info("Getting {} phone", phone);
        return phone;
    }

    public void setPhone(String phone) {
        logger.info("Setting {} phone to {} phone", phone, this.phone);
        this.phone = phone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Phone phone1 = (Phone) o;
        return phone.equals(phone1.phone);
    }

    @Override
    public int hashCode() {
        return Objects.hash(phone);
    }

    @Override
    public int compareTo(Phone o) {
        return (phone.compareTo(o.getPhone()));
    }

    @Override
    public String toString() {
        return "Phone{" +
                "phone='" + phone + '\'' +
                '}';
    }

}
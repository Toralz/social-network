package com.getjavajob.training.siavrisi.socialnetwork.adapter;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Utility class for date adapting during XML up/download
 */
public class DateAdapter extends XmlAdapter<String, Date> {

    private static final String CUSTOM_FORMAT_STRING = "MM/dd/yyyy";

    @Override
    public String marshal(Date date) {
        return new SimpleDateFormat(CUSTOM_FORMAT_STRING).format(date);
    }

    @Override
    public Date unmarshal(String date) throws ParseException {
        return new Date(new SimpleDateFormat(CUSTOM_FORMAT_STRING).parse(date).getTime());
    }

}

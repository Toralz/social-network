package com.getjavajob.training.siavrisi.socialnetwork.domain;

import com.getjavajob.training.siavrisi.socialnetwork.dto.CommunityDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@NamedQuery(name = "Community.selectByName",
        query = "SELECT c from Community c WHERE c.name = :name")
public class Community {

    private static Logger logger = LoggerFactory.getLogger(Community.class);
    @ManyToMany(mappedBy = "communities", fetch = FetchType.LAZY)
    List<Account> accounts = new ArrayList<>();
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private String description;
    private int creatorId;
    @Lob
    private Blob photo;

    public Community() {
    }

    public Community(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Community(int id, String name, String description) {
        this(name, description);
        this.id = id;
    }

    public Community(int id, String name, String description, int creatorId) {
        this(id, name, description);
        this.creatorId = creatorId;
    }

    public Community(CommunityDTO communityDTO) {
        this.id = communityDTO.getId();
        this.name = communityDTO.getName();
        this.description = communityDTO.getDescription();
        this.creatorId = communityDTO.getCreatorId();
    }

    public Blob getPhoto() {
        logger.info("Getting photo of {} community", id);
        return photo;
    }

    public void setPhoto(Blob photo) {
        logger.info("Setting photo to {} community", id);
        this.photo = photo;
    }

    public int getId() {
        logger.info("Getting id of {} community", id);
        return id;
    }

    public void setId(int id) {
        logger.info("Setting {} id to {} community", id, this.id);
        this.id = id;
    }

    public String getName() {
        logger.info("Getting name of {} community", id);
        return name;
    }

    public void setName(String name) {
        logger.info("Setting {} name to {} community", name, id);
        this.name = name;
    }

    public String getDescription() {
        logger.info("Getting description of {} community", id);
        return description;
    }

    public void setDescription(String description) {
        logger.info("Setting {} description to {} community", description, id);
        this.description = description;
    }

    public int getCreatorId() {
        logger.info("Getting creatorId of {} community", id);
        return creatorId;
    }

    public void setCreatorId(int creatorId) {
        logger.info("Setting {} creatorId to {} community", creatorId, id);
        this.creatorId = creatorId;
    }

    public List<Account> getAccounts() {
        logger.info("Getting members of {} community", id);
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        logger.info("Setting members to {} community", id);
        this.accounts = accounts;
    }

    @Override
    public String toString() {
        return "Community{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Community community = (Community) o;
        return id == community.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

}

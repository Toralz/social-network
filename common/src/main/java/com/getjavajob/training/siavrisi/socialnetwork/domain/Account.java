package com.getjavajob.training.siavrisi.socialnetwork.domain;

import com.getjavajob.training.siavrisi.socialnetwork.dto.AccountDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.sql.Blob;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

@Entity
@Table(name = "account")
@NamedQuery(name = "Account.selectByEmail",
        query = "SELECT a FROM Account a WHERE a.email = :email")
public class Account {

    private static Logger logger = LoggerFactory.getLogger(Account.class);
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "communitymembership",
            joinColumns = @JoinColumn(name = "userID"),
            inverseJoinColumns = @JoinColumn(name = "communityID"))
    List<Community> communities = new ArrayList<>();
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "userID")
    private int id;
    private String firstName;
    private String lastName;
    private String patronymic;
    @Column(name = "skype", unique = true)
    private String skype;
    @Column(name = "ICQ", unique = true)
    private String icq;
    @Column(name = "email", unique = true)
    private String email;
    private String homeAddress;
    private String workAddress;
    private String extra;
    private String password;
    private Date birthday;
    @Lob
    private Blob photo;
    @OneToMany(targetEntity = Phone.class, cascade = CascadeType.ALL,
            fetch = FetchType.LAZY, orphanRemoval = true)
    @JoinColumn(name = "accountId", referencedColumnName = "userID")
    private Set<Phone> phones = new TreeSet<>();

    public Account(AccountDTO accountDTO) {
        this.id = accountDTO.getId();
        this.firstName = accountDTO.getFirstName();
        this.lastName = accountDTO.getLastName();
        this.patronymic = accountDTO.getPatronymic();
        this.skype = accountDTO.getSkype();
        this.icq = accountDTO.getIcq();
        this.email = accountDTO.getEmail();
        this.homeAddress = accountDTO.getHomeAddress();
        this.workAddress = accountDTO.getWorkAddress();
        this.extra = accountDTO.getExtra();
        this.birthday = accountDTO.getBirthday();
        this.password = accountDTO.getPassword();
        if (accountDTO.getPhones() != null) {
            for (String phone : accountDTO.getPhones()) {
                this.addPhone(new Phone(phone));
            }
        }
    }

    public Account() {

    }

    public Account(int id) {
        this.id = id;
    }

    public Account(int id, String firstName) {
        this(id);
        this.firstName = firstName;
    }

    public Account(int id, String firstName, String lastName) {
        this(id, firstName);
        this.lastName = lastName;
    }

    public Account(int id, String firstName, String lastName, String patronymic) {
        this(id, firstName, lastName);
        this.patronymic = patronymic;
    }

    public Account(int id, String firstName, String lastName, String patronymic, String skype) {
        this(id, firstName, lastName, patronymic);
        this.skype = skype;
    }

   public Account(int id, String firstName, String lastName, String patronymic, String skype, String icq) {
        this(id, firstName, lastName, patronymic, skype);
        this.icq = icq;
   }

   public Account(int id, String firstName, String lastName, String patronymic, String skype, String icq,
                  String homeAddress) {
        this(id, firstName, lastName, patronymic, skype, icq);
        this.homeAddress = homeAddress;
   }

   public Account(int id, String firstName, String lastName, String patronymic, String skype, String icq,
                  String homeAddress, String workAddress) {
        this(id, firstName, lastName, patronymic, skype, icq, homeAddress);
        this.workAddress = workAddress;
   }

   public Account(int id, String firstName, String lastName, String patronymic, String skype, String icq,
                  String homeAddress, String workAddress, String extra) {
        this(id, firstName, lastName, patronymic, skype, icq, homeAddress, workAddress);
        this.extra = extra;
   }

   public Account(int id, String firstName, String lastName, String patronymic, String skype, String icq,
                  String homeAddress, String workAddress, String extra, String password) {
        this(id, firstName, lastName, patronymic, skype, icq, homeAddress, workAddress, extra);
        this.password = password;
   }

   public Account(int id, String firstName, String lastName, String patronymic, String skype, String icq,
                  String homeAddress, String workAddress, String extra, String password, Date birthday) {
        this(id, firstName, lastName, patronymic, skype, icq, homeAddress, workAddress, extra, password);
        this.birthday = birthday;
   }

    public Blob getPhoto() {
        logger.info("Getting photo of {} account", id);
        return photo;
    }

    public void setPhoto(Blob photo) {
        logger.info("Setting photo to {} account", id);
        this.photo = photo;
    }

    public Set<Phone> getPhones() {
        logger.info("Getting phones of {} account", id);
        return phones;
    }

    public void setPhones(Set<Phone> phones) {
        logger.info("Setting phones {} to {} account", phones, id);
        this.phones = phones;
    }

    public void addPhone(Phone phone) {
        logger.info("Adding {} phone to {} account", phone, id);
        phones.add(phone);
    }

    public void deletePhone(Phone phone) {
        logger.info("Deleting {} phone to {} account", phone, id);
        phones.remove(phone);
    }

    public List<Community> getCommunities() {
        logger.info("Getting communities of {} account", id);
        return communities;
    }

    public void setCommunities(List<Community> communities) {
        logger.info("Setting communities to {} account", id);
        this.communities = communities;
    }

    public String getHomeAddress() {
        logger.info("Getting homeAddress of {} account", id);
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        logger.info("Setting {} homeAddress to {} account", homeAddress, id);
        this.homeAddress = homeAddress;
    }

    public String getWorkAddress() {
        logger.info("Getting workAddress of {} account", id);
        return workAddress;
    }

    public void setWorkAddress(String workAddress) {
        logger.info("Setting {} workAddress to {} account", workAddress, id);
        this.workAddress = workAddress;
    }

    public Date getBirthday() {
        logger.info("Getting birthday of {} account", id);
        return birthday;
    }

    public void setBirthday(Date birthday) {
        logger.info("Setting {} birthday to {} account", birthday, id);
        this.birthday = birthday;
    }

    public String getFirstName() {
        logger.info("Getting firstName of {} account", id);
        return firstName;
    }

    public void setFirstName(String firstName) {
        logger.info("Setting {} firstName to {} account", firstName, id);
        this.firstName = firstName;
    }

    public String getLastName() {
        logger.info("Getting lastName of {} account", id);
        return lastName;
    }

    public void setLastName(String lastName) {
        logger.info("Setting {} lastName to {} account", lastName, id);
        this.lastName = lastName;
    }

    public String getPatronymic() {
        logger.info("Getting patronymic of {} account", id);
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        logger.info("Setting {} patronymic to {} account", patronymic, id);
        this.patronymic = patronymic;
    }

    public String getSkype() {
        logger.info("Getting skype of {} account", id);
        return skype;
    }

    public void setSkype(String skype) {
        logger.info("Setting {} skype to {} account", skype, id);
        this.skype = skype;
    }

    public String getEmail() {
        logger.info("Getting email of {} account", id);
        return email;
    }

    public void setEmail(String email) {
        logger.info("Setting {} email to {} account", email, id);
        this.email = email;
    }

    public String getIcq() {
        logger.info("Getting icq of {} account", id);
        return icq;
    }

    public void setIcq(String icq) {
        logger.info("Setting {} icq to {} account", icq, id);
        this.icq = icq;
    }

    public String getExtra() {
        logger.info("Getting extra of {} account", id);
        return extra;
    }

    public void setExtra(String extra) {
        logger.info("Setting {} extra to {} account", extra, id);
        this.extra = extra;
    }

    public int getId() {
        logger.info("Getting id of {} account", id);
        return id;
    }

    public void setId(int id) {
        logger.info("Setting {} id to {} account", id, this.id);
        this.id = id;
    }

    public String getPassword() {
        logger.info("Getting password of {} account", id);
        return password;
    }

    public void setPassword(String password) {
        logger.info("Setting {} password to {} account", password, id);
        this.password = password;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", skype='" + skype + '\'' +
                ", icq='" + icq + '\'' +
                ", email='" + email + '\'' +
                ", homeAddress='" + homeAddress + '\'' +
                ", workAddress='" + workAddress + '\'' +
                ", extra='" + extra + '\'' +
                ", password='" + password + '\'' +
                ", birthday=" + birthday +
                ", phones=" + phones +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Account account = (Account) o;
        return id == account.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

}

package com.getjavajob.training.siavrisi.socialnetwork.dto;

import com.getjavajob.training.siavrisi.socialnetwork.adapter.DateAdapter;
import com.getjavajob.training.siavrisi.socialnetwork.domain.Account;
import com.getjavajob.training.siavrisi.socialnetwork.domain.Phone;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.sql.Date;
import java.util.Set;
import java.util.TreeSet;

@XmlRootElement(name = "account")
@XmlAccessorType(XmlAccessType.FIELD)
public class AccountDTO {

    @XmlTransient
    private int id;
    private String firstName;
    private String lastName;
    private String patronymic;
    private String skype;
    private String icq;
    private String email;
    private String homeAddress;
    private String workAddress;
    private String extra;
    @XmlTransient
    private String password;
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date birthday;
    @XmlElementWrapper
    @XmlElement(name = "phone")
    private Set<String> phones;

    public AccountDTO() {

    }

    public AccountDTO(Account account) {
        this.id = account.getId();
        this.firstName = account.getFirstName();
        this.lastName = account.getLastName();
        this.patronymic = account.getPatronymic();
        this.skype = account.getSkype();
        this.icq = account.getIcq();
        this.email = account.getEmail();
        this.homeAddress = account.getHomeAddress();
        this.workAddress = account.getWorkAddress();
        this.extra = account.getExtra();
        this.password = account.getPassword();
        this.birthday = account.getBirthday();
        this.phones = new TreeSet<>();
        for (Phone phone : account.getPhones()) {
            phones.add(phone.getPhone());
        }
    }

    public AccountDTO(int id, String firstName, String lastName, String patronymic, String skype,
                      String icq, String email, String homeAddress,
                      String workAddress, String extra, String password, Date birthday) {
        this(firstName, lastName, patronymic, skype, icq, email, homeAddress, workAddress, extra, password, birthday);
        this.id = id;
    }

    public AccountDTO(String firstName, String lastName,
                      String patronymic, String skype, String icq, String email, String homeAddress,
                      String workAddress, String extra, String password, Date birthday) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.skype = skype;
        this.icq = icq;
        this.email = email;
        this.homeAddress = homeAddress;
        this.workAddress = workAddress;
        this.extra = extra;
        this.password = password;
        this.birthday = birthday;
    }

    public Set<String> getPhones() {
        return phones;
    }

    public void setPhones(Set<String> phones) {
        this.phones = phones;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getIcq() {
        return icq;
    }

    public void setIcq(String icq) {
        this.icq = icq;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getWorkAddress() {
        return workAddress;
    }

    public void setWorkAddress(String workAddress) {
        this.workAddress = workAddress;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    @Override
    public String toString() {
        return "AccountDTO{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", skype='" + skype + '\'' +
                ", icq='" + icq + '\'' +
                ", email='" + email + '\'' +
                ", homeAddress='" + homeAddress + '\'' +
                ", workAddress='" + workAddress + '\'' +
                ", extra='" + extra + '\'' +
                ", password='" + password + '\'' +
                ", birthday=" + birthday +
                ", phones=" + phones +
                '}';
    }

}

package com.getjavajob.training.siavrisi.socialnetwork.domain;

import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "friendship")
public class Friendship {

    @ManyToOne
    @MapsId("accountFromId")
    private Account accountFrom;
    @ManyToOne
    @MapsId("accountToId")
    private Account accountTo;
    @EmbeddedId
    private Key key = new Key();
    private boolean accepted;

    public Key getKey() {
        return key;
    }

    public void setKey(Key key) {
        this.key = key;
    }

    public Account getAccountFrom() {
        return accountFrom;
    }

    public void setAccountFrom(Account accountFrom) {
        this.accountFrom = accountFrom;
    }

    public Account getAccountTo() {
        return accountTo;
    }

    public void setAccountTo(Account accountTo) {
        this.accountTo = accountTo;
    }

    public boolean isAccepted() {
        return accepted;
    }

    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }

    @Embeddable
    public static class Key implements Serializable {

        private int accountFromId;
        private int accountToId;

        public Key(int accountFromId, int accountToId) {
            this.accountFromId = accountFromId;
            this.accountToId = accountToId;
        }

        public Key() {

        }

        public int getAccountFromId() {
            return accountFromId;
        }

        public void setAccountFromId(int accountFromId) {
            this.accountFromId = accountFromId;
        }

        public int getAccountToId() {
            return accountToId;
        }

        public void setAccountToId(int accountToId) {
            this.accountToId = accountToId;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            Key key = (Key) o;
            return (accountFromId == key.accountFromId && accountToId == key.accountToId) ||
                    (accountFromId == key.accountToId && accountToId == key.accountFromId);
        }

        @Override
        public int hashCode() {
            return Objects.hash(accountFromId, accountToId);
        }

    }

}

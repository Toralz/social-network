package com.getjavajob.training.siavrisi.socialnetwork.dto;


import com.getjavajob.training.siavrisi.socialnetwork.domain.Community;

public class CommunityDTO {

    private int id;
    private String name;
    private String description;
    private int creatorId;

    public CommunityDTO() {
    }

    public CommunityDTO(Community community) {
        this.id = community.getId();
        this.name = community.getName();
        this.description = community.getDescription();
        if (community.getCreatorId() != 0) {
            this.creatorId = community.getCreatorId();
        }
    }

    public CommunityDTO(int id, String name, String description, int creatorId) {
        this(name, description, creatorId);
        this.id = id;
    }

    public CommunityDTO(String name, String description, int creatorId) {
        this.name = name;
        this.description = description;
        this.creatorId = creatorId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(int creatorId) {
        this.creatorId = creatorId;
    }

}

package com.getjavajob.training.siavrisi.socialnetwork.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement(proxyTargetClass = true)
@Import(DaoConfiguration.class)
public class ServiceConfig {

    private final DaoConfiguration daoConfiguration;

    @Autowired
    public ServiceConfig(DaoConfiguration daoConfiguration) {
        this.daoConfiguration = daoConfiguration;
    }

    @Bean
    public PlatformTransactionManager transactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(daoConfiguration.entityManagerFactory().getObject());
        return transactionManager;
    }

}

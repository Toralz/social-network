package com.getjavajob.training.siavrisi.socialnetwork.service;

import com.getjavajob.training.siavrisi.socialnetwork.dao.AccountDao;
import com.getjavajob.training.siavrisi.socialnetwork.domain.Account;
import com.getjavajob.training.siavrisi.socialnetwork.model.AccountDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Service class to do some logic with {@link com.getjavajob.training.siavrisi.socialnetwork.model.AccountDetails}
 */
@Service
public class AccountDetailsService implements UserDetailsService {

    private static final Logger logger = LoggerFactory.getLogger(AccountDetailsService.class);

    private final AccountDao accountRepository;

    @Autowired
    public AccountDetailsService(AccountDao accountRepository) {
        this.accountRepository = accountRepository;
    }

    /**
     * Loads account from database with {@code accountDao} based on his {@code email}
     *
     * @param email of the account
     * @return userDetails created using account's email, id and password
     */
    @Override
    @Transactional
    public UserDetails loadUserByUsername(String email) {
        logger.info("in loadUserByUsername with {}", email);
        Account account = accountRepository.getByEmail(email);
        List<GrantedAuthority> list = new ArrayList<>();
        list.add(new SimpleGrantedAuthority("ROLE_USER"));
        return buildUserForAuthentication(account, list);
    }

    /**
     * This method is used by
     * {@link com.getjavajob.training.siavrisi.socialnetwork.service.AccountDetailsService#loadUserByUsername(String email)}
     *
     * @param account   to get email, password and id from
     * @param authority to grant
     * @return userDetails created using account's email, in and password
     */
    private UserDetails buildUserForAuthentication(Account account, List<GrantedAuthority> authority) {
        logger.info("in buildUserForAuthentication {} {} {}", account.getEmail(), account.getPassword(),
                account.getId());
        return new AccountDetails(account.getEmail(), account.getPassword(), authority, account.getId());
    }


}

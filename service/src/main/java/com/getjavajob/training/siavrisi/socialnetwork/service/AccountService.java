package com.getjavajob.training.siavrisi.socialnetwork.service;

import com.getjavajob.training.siavrisi.socialnetwork.dao.AccountDao;
import com.getjavajob.training.siavrisi.socialnetwork.dao.FriendshipDao;
import com.getjavajob.training.siavrisi.socialnetwork.domain.Account;
import com.getjavajob.training.siavrisi.socialnetwork.domain.Friendship;
import com.getjavajob.training.siavrisi.socialnetwork.domain.Phone;
import com.getjavajob.training.siavrisi.socialnetwork.dto.AccountDTO;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.sql.rowset.serial.SerialBlob;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.mindrot.jbcrypt.BCrypt.gensalt;
import static org.mindrot.jbcrypt.BCrypt.hashpw;

/**
 * Service class to work with accounts
 */
@Service
public class AccountService {

    private static final Logger logger = LoggerFactory.getLogger(AccountService.class);

    private final AccountDao accountRepository;

    private final FriendshipDao friendshipRepository;

    @Autowired
    public AccountService(AccountDao accountRepository, FriendshipDao friendshipRepository) {
        this.accountRepository = accountRepository;
        this.friendshipRepository = friendshipRepository;
    }

    /**
     * Utility method to convert {@code account} to xml
     *
     * @param account account to be converted
     * @return xml file
     */
    private static File accountToXml(AccountDTO account) {
        logger.info("Creating account from xml file");
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(AccountDTO.class);
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            File file = new File("Account" + account.getId() + ".xml");
            marshaller.marshal(account, file);
            return file;
        } catch (JAXBException e) {
            logger.error("Error occurred during conversion {} account to xml", account);
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Utility method to convert {@code xml} to account
     *
     * @param xml xml file
     * @return account
     * @throws IOException
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws JAXBException
     * @throws ParseException
     */
    private static Account accountFromXml(File xml) throws IOException, SAXException, ParserConfigurationException,
            JAXBException, ParseException {
        JAXBContext jaxbContext = JAXBContext.newInstance(AccountDTO.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        builderFactory.setNamespaceAware(true);
        DocumentBuilder builder = builderFactory.newDocumentBuilder();
        Document document = builder.parse(xml);
        AccountDTO accountDTO = (AccountDTO) unmarshaller.unmarshal(document);
        return new Account(accountDTO);
    }

    /**
     * This method validates account using
     * {@link com.getjavajob.training.siavrisi.socialnetwork.service.AccountService#validateCreateAccount(Account account)} and
     * {@link com.getjavajob.training.siavrisi.socialnetwork.service.AccountService#validatePhoneNumber(String phone)}
     * and inserts it into database in case of success
     *
     * @param accountDTO  to create
     * @param phoneNumber to insert into account
     * @return true if account created successfully, false if not
     */
    @Transactional(rollbackFor = {NumberFormatException.class, SQLException.class})
    public boolean createAccount(AccountDTO accountDTO, String phoneNumber) {
        Account account = new Account(accountDTO);
        String password = accountDTO.getPassword();
        password = hashpw(password, gensalt());
        account.setPassword(password);
        logger.info("Account creation for {} account with {} phoneNumber", account, phoneNumber);
        if (validateCreateAccount(account)) {
            logger.info("Account validated");
            nullEmptySkypeIcq(account);             //nulling empty skype and icq to not get unique field violation
            // in db
            try {
                if (validatePhoneNumber(phoneNumber)) {
                    Phone phone = new Phone(phoneNumber);
                    account.addPhone(phone);
                    logger.info("Account created successfully");
                }
                accountRepository.save(account);
                return true;
            } catch (NumberFormatException e) {
                logger.error("Exception has been thrown during account creation");
                e.printStackTrace();
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * This method uses {@code friendshipRepository to create new friendship request between {@code accountFrom}} and
     * {@code accountTo}
     *
     * @param accountFrom account id to send request from
     * @param accountTo   account id to get request
     */
    @Transactional
    public void addFriend(int accountFrom, int accountTo) {
        Friendship friendship = new Friendship();
        friendship.setAccepted(false);
        friendship.setKey(new Friendship.Key(accountFrom, accountTo));
        friendship.setAccountFrom(getById(accountFrom));
        friendship.setAccountTo(getById(accountTo));
        friendshipRepository.insert(friendship);
    }

    /**
     * This method deletes friendship between {@code accountFrom} and {@code accountTo}
     *
     * @param accountFrom id
     * @param accountTo   id
     */
    @Transactional
    public void deleteFriend(int accountFrom, int accountTo) {
        friendshipRepository.delete(new Friendship.Key(accountFrom, accountTo));
        friendshipRepository.delete(new Friendship.Key(accountTo, accountFrom));
    }

    /**
     * Accepts friendship request between {@code accountFrom} and {@code accountTo}
     *
     * @param accountFrom id
     * @param accountTo   id
     */
    @Transactional
    public void acceptFriend(int accountFrom, int accountTo) {
        Friendship friendship = friendshipRepository.get(new Friendship.Key(accountFrom, accountTo));
        friendship.setAccepted(true);
        friendshipRepository.insert(friendship);

    }

    /**
     * Gets all unaccepted requests going from {@code accountFromId}
     *
     * @param accountFromId id
     * @return list of AccountDTO
     */
    @Transactional
    public List<AccountDTO> getUnacceptedFriendsFrom(int accountFromId) {
        List<Friendship> friendships = friendshipRepository.getByAccountFromAccepted(accountFromId,
                false);
        List<AccountDTO> accountDTOS = new ArrayList<>();
        extractAccountsTo(friendships, accountDTOS);
        return accountDTOS;
    }

    /**
     * Gets all unaccepted requests going to {@code accountToId}
     *
     * @param accountToId id
     * @return list of AccountDTO
     */
    @Transactional
    public List<AccountDTO> getUnacceptedFriendsTo(int accountToId) {
        List<Friendship> friendships = friendshipRepository.getByAccountToAccepted(accountToId, false);
        List<AccountDTO> accountDTOS = new ArrayList<>();
        extractAccountsFrom(friendships, accountDTOS);
        return accountDTOS;
    }

    /**
     * Gets all accepted requests of {@code accountId}
     *
     * @param accountId id
     * @return list of AccountDTO
     */
    @Transactional
    public List<AccountDTO> getAcceptedFriends(int accountId) {
        List<Friendship> friendshipsTo = friendshipRepository.getByAccountToAccepted(accountId, true);
        List<Friendship> friendshipsFrom = friendshipRepository.getByAccountFromAccepted(accountId,
                true);
        List<AccountDTO> accountDTOS = new ArrayList<>();
        extractAccountsFrom(friendshipsTo, accountDTOS);
        extractAccountsTo(friendshipsFrom, accountDTOS);
        return accountDTOS;
    }

    /**
     * Checks if a friendship request exists between {@code accountFrom} and {@code accountTo}
     *
     * @param accountFrom id
     * @param accountTo   id
     * @return true if friendship request exists, false otherwise
     */
    @Transactional
    public boolean isFriends(int accountFrom, int accountTo) {
        return friendshipRepository.get(new Friendship.Key(accountFrom, accountTo)).getKey().getAccountFromId() != 0 ||
                friendshipRepository.get(new Friendship.Key(accountTo, accountFrom)).getKey().getAccountFromId() != 0;
    }

    /**
     * Utility method to extract accounts from list of friendships
     *
     * @param friendshipsFrom list of friendships
     * @param accountDTOS     list of accountDTOs
     */
    private void extractAccountsTo(List<Friendship> friendshipsFrom, List<AccountDTO> accountDTOS) {
        for (Friendship friendship : friendshipsFrom) {
            Account account = friendship.getAccountTo();
            AccountDTO accountDTO = new AccountDTO();
            accountDTO.setId(account.getId());
            accountDTO.setFirstName(account.getFirstName());
            accountDTO.setLastName(account.getLastName());
            accountDTO.setPatronymic(account.getPatronymic());
            accountDTOS.add(accountDTO);
        }
    }

    /**
     * Utility method to extract accounts from list of friendships
     *
     * @param friendshipsTo list of friendships
     * @param accountDTOS   list of accountDTOs
     */
    private void extractAccountsFrom(List<Friendship> friendshipsTo, List<AccountDTO> accountDTOS) {
        for (Friendship friendship : friendshipsTo) {
            Account account = friendship.getAccountFrom();
            AccountDTO accountDTO = new AccountDTO();
            accountDTO.setId(account.getId());
            accountDTO.setFirstName(account.getFirstName());
            accountDTO.setLastName(account.getLastName());
            accountDTO.setPatronymic(account.getPatronymic());
            accountDTOS.add(accountDTO);
        }
    }

    /**
     * Method to update {@code id} account with {@code account}, add {@code phones} and remove @{@code removedPhones}
     *
     * @param id            id
     * @param account       to be updated with
     * @param phones        to be added
     * @param removedPhones to be removed
     * @return true if updated successfully, false otherwise
     */
    @Transactional(rollbackFor = {NumberFormatException.class, SQLException.class})
    public boolean updateAccount(int id, Account account, String[] phones, String[] removedPhones) {
        logger.info("Account update for {} id with {} account, adding {} phones and removing {} removedPhones", id,
                account, phones, removedPhones);
        Account oldAccount = getById(id);
        account.setPhones(oldAccount.getPhones());
        account.setCommunities(oldAccount.getCommunities());
        account.setId(id);
        if (validateUpdateAccount(account)) {
            logger.info("Account validated");
            nullEmptySkypeIcq(account);
            String password = account.getPassword();
            String oldPassword = getById(id).getPassword();
            if (password != null) {
                if (! password.isEmpty()) {
                    if (! password.equals(oldPassword)) {
                        password = hashpw(password, gensalt());
                    }
                } else {
                    password = oldPassword;
                }
            }
            account.setPassword(password);
            try {
                if (validatePhoneNumber(phones)) {
                    for (String phone : phones) {
                        logger.info("Adding {} phone", phone);
                        account.addPhone(new Phone(phone));
                    }
                }
                if (removedPhones != null) {
                    for (String phone : removedPhones) {
                        logger.info("Removing {} phone", phone);
                        account.deletePhone(new Phone(phone));
                    }
                }
                accountRepository.save(account);
                return true;
            } catch (NumberFormatException e) {
                logger.error("Exception has thrown during account update");
                e.printStackTrace();
                return false;
            }
        } else {
            logger.error("Account has not been validated");
            return false;
        }
    }

    /**
     * Method to delete account {@code id} by id
     *
     * @param id id
     * @return true if deleted successfully, false otherwise
     */
    @Transactional(rollbackFor = SQLException.class)
    public boolean deleteAccount(int id) {
        logger.info("Deleting {} account", id);
        accountRepository.delete(id);
        return true;
    }

    /**
     * Method to get account by {@code id}
     *
     * @param id id
     * @return account from db
     */
    @Transactional
    public AccountDTO getAccountById(int id) {
        logger.info("Getting {} account", id);
        Account account = getById(id);
        Hibernate.initialize(account.getPhones());
        return new AccountDTO(account);
    }

    public Account getById(int id) {
        return accountRepository.get(id);
    }

    /**
     * Method to get account by {@code email}
     *
     * @param email email
     * @return account from db
     */
    public Account getAccountByEmail(String email) {
        logger.info("Getting {} by email", email);
        return accountRepository.getByEmail(email);
    }

    /**
     * Method to get count how much records in db has been found by given part
     *
     * @param part to be searched like
     * @return count as int
     */
    public int getCount(String part) {
        return accountRepository.getCount(part);
    }

    /**
     * Method to get all accounts
     *
     * @return list of all accounts from db
     */
    public List<Account> getAccounts() {
        logger.info("Getting all accounts");
        List<Account> accounts = new ArrayList<>();
        for (Account account : accountRepository.getAll()) {
            accounts.add(account);
        }
        return accounts;
    }

    /**
     * Method to convert account information to xml
     *
     * @param id id
     * @return xml file
     */
    @Transactional
    public File getAccountXml(int id) {
        Account account = getById(id);
        Hibernate.initialize(account.getPhones());
        AccountDTO accountDTO = new AccountDTO(account);
        return accountToXml(accountDTO);
    }

    /**
     * Get phones of {@code id} account
     *
     * @param id id
     * @return set of phones
     */
    @Transactional
    public Set<Phone> getPhones(int id) {
        logger.info("Getting phones of {} account", id);
        Account account = getById(id);
        Hibernate.initialize(account.getPhones());
        return account.getPhones();
    }

    /**
     * Method to update account with given xml file
     *
     * @param id  id
     * @param xml file
     * @return true if updated successfully, false otherwise
     */
    @Transactional
    public boolean updateAccountFromXml(int id, File xml) {
        logger.info("Updating {} account from xml", id);
        try {
            Account account = accountFromXml(xml);
            Account oldAccount = getById(id);
            account.setEmail(oldAccount.getEmail());
            account.setPassword(oldAccount.getPassword());
            account.setPhoto(oldAccount.getPhoto());
            accountRepository.save(account);
            return true;
        } catch (IOException | SAXException | ParserConfigurationException | JAXBException | ParseException e) {
            logger.error("Exception has been thrown during account update from xml");
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Method to search account with firstName, lastName or patronymic like {@code part} at {@code page}
     *
     * @param part to search accounts like
     * @param page to get accounts from
     * @return list of accountDTOs
     */
    public List<AccountDTO> getAccountsLike(String part, int page) {
        logger.info("Getting accounts like {} on {} page", part, page);
        List<Account> accounts =
                accountRepository.getLike(part, page - 1);
        List<AccountDTO> accountDTOS = new ArrayList<>();
        for (Account account : accounts) {
            AccountDTO accountDTO = new AccountDTO();
            accountDTO.setId(account.getId());
            accountDTO.setFirstName(account.getFirstName());
            accountDTO.setLastName(account.getLastName());
            accountDTO.setPatronymic(account.getPatronymic());
            accountDTOS.add(accountDTO);
        }
        return accountDTOS;
    }

    /**
     * Method to set photo for {@code id}
     *
     * @param bytes of given blob
     * @param id    id
     * @return true if success, false otherwise
     */
    @Transactional(rollbackFor = SQLException.class)
    public boolean setPhoto(byte[] bytes, int id) {
        logger.info("Setting photo for {} id", id);
        if (bytes.length == 0 || id <= 0) {
            logger.error("Photo has not been validated");
            return false;
        } else {
            try {
                Account account = getById(id);
                account.setPhoto(new SerialBlob(bytes));
                logger.info("Set photo successfully");
                return true;
            } catch (SQLException e) {
                e.printStackTrace();
                return false;
            }
        }
    }

    /**
     * Method to get photo from the database
     *
     * @param id user id
     * @return photo as blob
     */
    @Transactional
    public Blob getPhoto(int id) {
        logger.info("Getting photo for {} id", id);
        Account account = getById(id);
        return account.getPhoto();
    }

    /**
     * Method to validate account at creation stage
     *
     * @param account to be validated
     * @return true if validated, false otherwise
     */
    private boolean validateCreateAccount(Account account) {
        logger.info("Validating {} account on creation", account);
        return ! account.getPassword().isEmpty() && ! account.getEmail().isEmpty() &&
                ! account.getLastName().isEmpty() &&
                ! account.getFirstName().isEmpty();
    }

    /**
     * Method to validate account at updating stage
     *
     * @param account to be validated
     * @return true if validated, false otherwise
     */
    private boolean validateUpdateAccount(Account account) {
        logger.info("Validating {} account on update", account);
        return ! account.getFirstName().isEmpty() && ! account.getEmail().isEmpty() &&
                ! account.getLastName().isEmpty();
    }

    /**
     * Method to validate phone number
     *
     * @param phone to be validated
     * @return true if validated, false otherwise
     * @throws NumberFormatException if phone number is not formatted properly
     */
    @Transactional
    boolean validatePhoneNumber(String phone) throws NumberFormatException {
        logger.info("Validating {} phone", phone);
        if (phone.isEmpty()) {
            logger.debug("Phone is empty");
            return false;
        }
        String patternString = "^\\+\\d{11}$";
        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(phone);
        for (Account account : accountRepository.getAll()) {
            if (account.getPhones().contains(new Phone(phone))) {
                logger.error("Duplicated phone found");
                throw new NumberFormatException("Wrong phoneNumber number");
            }
        }
        if (! (matcher.matches())) {
            logger.error("Phone is not formatted properly");
            throw new NumberFormatException("Wrong phoneNumber number");
        }
        logger.info("Phone number validated");
        return true;
    }

    /**
     * Method to an array of phones using
     * {@link com.getjavajob.training.siavrisi.socialnetwork.service.AccountService#validatePhoneNumber(String[])}
     *
     * @param phones to be validated
     * @return true if validated, false otherwise
     * @throws NumberFormatException if phone number is not formatted properly
     */
    private boolean validatePhoneNumber(String[] phones) throws NumberFormatException {
        logger.info("Validating array of phones {}", (Object) phones);
        if (phones == null || phones.length == 0) {
            logger.error("Error occurred: phones are empty");
            return false;
        }
        for (String phone : phones) {
            validatePhoneNumber(phone);
        }
        logger.info("Phones are validated");
        return true;
    }

    /**
     * Utility method to set null values to empty skype and icq
     *
     * @param account to be checked for empty skype and icq
     */
    private void nullEmptySkypeIcq(Account account) {
        logger.info("Nulling empty skype and icq");
        if (account.getSkype().isEmpty()) {
            account.setSkype(null);
        }
        if (account.getIcq().isEmpty()) {
            account.setIcq(null);
        }
    }

}

package com.getjavajob.training.siavrisi.socialnetwork.service;

import com.getjavajob.training.siavrisi.socialnetwork.dao.CommunityDao;
import com.getjavajob.training.siavrisi.socialnetwork.domain.Account;
import com.getjavajob.training.siavrisi.socialnetwork.domain.Community;
import com.getjavajob.training.siavrisi.socialnetwork.dto.CommunityDTO;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.rowset.serial.SerialBlob;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Service
public class CommunityService {

    private static final Logger logger = LoggerFactory.getLogger(CommunityService.class);

    private final CommunityDao communityRepository;

    @Autowired
    public CommunityService(CommunityDao communityRepository) {
        this.communityRepository = communityRepository;
    }

    /**
     * Method to check if given {@code userId} is a creator of {@code communityId}
     *
     * @param communityId community id
     * @param userId      user id
     * @return true if creator, false otherwise
     */
    public boolean checkCreator(int communityId, int userId) {
        logger.info("Checking if {} account is creator of {} community", userId, communityId);
        Community community = getById(communityId);
        return community.getCreatorId() == userId;
    }

    /**
     * Method to get community by {@code name}
     *
     * @param name name of community
     * @return community from db
     */
    public Community getByName(String name) {
        logger.info("Getting community by {} name", name);
        return communityRepository.getByName(name);
    }

    Community getById(int id) {
        logger.info("Getting community by {} id", id);
        return communityRepository.get(id);
    }

    /**
     * Method to get count how much records in db has been found by given part
     *
     * @param part to be searched like
     * @return count as int
     */
    public int getCount(String part) {
        return communityRepository.getCount(part);
    }

    /**
     * Method to validate given community using
     * {@link com.getjavajob.training.siavrisi.socialnetwork.service.CommunityService#validateCommunity(Community)} and
     * insert it into db
     *
     * @param community to be created
     * @return true if inserted successfully
     */
    @Transactional
    public boolean createCommunity(Community community) {
        logger.info("Community creation for {}", community);
        if (validateCommunity(community)) {
            communityRepository.save(community);
            logger.info("Community created");
            return true;
        } else {
            logger.error("Community has not been validated");
            return false;
        }
    }

    /**
     * Method to set given {@code inputStream} as blob for community with {@code id}
     *
     * @param bytes of blob
     * @param id    community id
     * @return true if success
     */
    @Transactional
    public boolean setPhoto(byte[] bytes, int id) {
        logger.info("Setting photo for {} community", id);
        if (bytes.length == 0 || id <= 0) {
            logger.error("Photo has not been validated");
            return false;
        } else {
            try {
                Community community = getById(id);
                community.setPhoto(new SerialBlob(bytes));
                logger.info("Set photo successfully");
                return true;
            } catch (SQLException e) {
                e.printStackTrace();
                return false;
            }
        }
    }

    /**
     * Method to get photo of community with {@code id} as blob
     *
     * @param id community id
     * @return blob
     */
    @Transactional
    public Blob getPhoto(int id) {
        logger.info("Getting photo for {} community", id);
        return getById(id).getPhoto();
    }

    /**
     * Method to delete community with {@code id}
     *
     * @param id id
     * @return true if success
     */
    @Transactional
    public boolean deleteCommunity(int id) {
        logger.info("Deleting {} community", id);
        Community community = getById(id);
        Hibernate.initialize(community.getAccounts());
        for (Account account : community.getAccounts()) {
            account.getCommunities().remove(community);
        }
        communityRepository.save(community);
        communityRepository.delete(id);
        return true;
    }

    /**
     * Method to update community with {@code id} with {@code community}
     *
     * @param id        community id to be updated
     * @param community to be updated with
     * @return true if success
     */
    @Transactional
    public boolean updateCommunity(int id, Community community) {
        logger.info("Updating {} community for {}", id, community);
        if (validateCommunity(community)) {
            community.setId(id);
            communityRepository.save(community);
            logger.info("Community has been updated");
            return true;
        } else {
            logger.error("Community has not been validated");
            return false;
        }
    }

    /**
     * Method to get all the communities from db
     *
     * @return list of communities
     */
    public List<Community> getCommunities() {
        logger.info("Getting all communities");
        return new ArrayList<>(communityRepository.getAll());
    }

    /**
     * Method to get community by {@code id}
     *
     * @param id community id
     * @return community from db
     */
    public Community getCommunity(int id) {
        logger.info("Getting {} community", id);
        return communityRepository.get(id);
    }

    /**
     * Method to get communities with name or description like {@code part} at {@code page}
     *
     * @param part to be searched like
     * @param page to be displayed at
     * @return list of communityDTOs
     */
    public List<CommunityDTO> getCommunityLike(String part, int page) {
        logger.info("Getting communities like {} on {} page", part, page);
        List<Community> communities = communityRepository.getLike(part,
                page - 1);
        List<CommunityDTO> communityDTOS = new ArrayList<>();
        for (Community community : communities) {
            CommunityDTO communityDTO = new CommunityDTO();
            communityDTO.setId(community.getId());
            communityDTO.setCreatorId(community.getCreatorId());
            communityDTO.setName(community.getName());
            communityDTO.setDescription(community.getDescription());
            communityDTOS.add(communityDTO);
        }
        return communityDTOS;
    }

    /**
     * Method to validate {@code community} (check if name is empty)
     *
     * @param community to be validated
     * @return true if validated
     */
    private boolean validateCommunity(Community community) {
        logger.info("Validating community");
        return ! community.getName().isEmpty();
    }

}

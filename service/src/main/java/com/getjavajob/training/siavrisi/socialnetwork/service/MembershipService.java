package com.getjavajob.training.siavrisi.socialnetwork.service;

import com.getjavajob.training.siavrisi.socialnetwork.dao.AccountDao;
import com.getjavajob.training.siavrisi.socialnetwork.dao.CommunityDao;
import com.getjavajob.training.siavrisi.socialnetwork.domain.Account;
import com.getjavajob.training.siavrisi.socialnetwork.domain.Community;
import com.getjavajob.training.siavrisi.socialnetwork.dto.AccountDTO;
import com.getjavajob.training.siavrisi.socialnetwork.dto.CommunityDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class MembershipService {

    private final AccountDao accountRepository;

    private final CommunityDao communityRepository;

    @Autowired
    public MembershipService(AccountDao accountRepository, CommunityDao communityRepository) {
        this.accountRepository = accountRepository;
        this.communityRepository = communityRepository;
    }

    @Transactional
    public List<CommunityDTO> getAccountCommunities(int id) {
        Account account = accountRepository.get(id);
        List<CommunityDTO> communityDTOS = new ArrayList<>();
        for (Community community : account.getCommunities()) {
            CommunityDTO communityDTO = new CommunityDTO();
            communityDTO.setId(community.getId());
            communityDTO.setName(community.getName());
            communityDTO.setDescription(community.getDescription());
            communityDTOS.add(communityDTO);
        }
        return communityDTOS;
    }

    @Transactional
    public List<AccountDTO> getCommunityAccounts(int id) {
        Community community = communityRepository.get(id);
        List<AccountDTO> accountDTOS = new ArrayList<>();
        for (Account account : community.getAccounts()) {
            AccountDTO accountDTO = new AccountDTO();
            accountDTO.setId(account.getId());
            accountDTO.setFirstName(account.getFirstName());
            accountDTO.setLastName(account.getLastName());
            accountDTO.setPatronymic(account.getPatronymic());
            accountDTOS.add(accountDTO);
        }
        return accountDTOS;
    }

    @Transactional
    public boolean checkMember(int accountId, int communityId) {
        Community community = communityRepository.get(communityId);
        Account account = accountRepository.get(accountId);
        return community.getAccounts().contains(account);
    }

    @Transactional
    public void addMember(int accountId, int communityId) {
        Community community = communityRepository.get(communityId);
        Account account = accountRepository.get(accountId);
        community.getAccounts().add(account);
        account.getCommunities().add(community);
        communityRepository.save(community);
    }

    @Transactional
    public void deleteMember(int accountId, int communityId) {
        Community community = communityRepository.get(communityId);
        Account account = accountRepository.get(accountId);
        account.getCommunities().remove(community);
        community.getAccounts().remove(account);
        communityRepository.save(community);
    }

}

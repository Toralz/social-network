package com.getjavajob.training.siavrisi.socialnetwork.service;

import com.getjavajob.training.siavrisi.socialnetwork.domain.Community;
import com.getjavajob.training.siavrisi.socialnetwork.repository.CommunityRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Blob;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CommunityServiceTest {

    @Mock
    private CommunityRepository communityRepository;

    private CommunityService communityService;

    @BeforeEach
    void setUp() {
        this.communityService = new CommunityService(communityRepository);
    }

    @Test
    void checkCreator() {
        Optional<Community> optionalCommunity = mock(Optional.class);
        Community community = new Community();
        community.setCreatorId(1);
        when(communityRepository.findById(anyInt())).thenReturn(optionalCommunity);
        when(optionalCommunity.orElse(any())).thenReturn(community);
        assertTrue(communityService.checkCreator(123, 1));
    }

    @Test
    void getByName() {
        Community community = new Community();
        community.setName("1");
        when(communityRepository.getByNameEquals(any())).thenReturn(community);
        assertEquals("1", communityService.getByName("1").getName());
    }

    @Test
    void getById() {
        Optional<Community> optionalCommunity = mock(Optional.class);
        Community community = new Community();
        community.setName("1");
        when(communityRepository.findById(anyInt())).thenReturn(optionalCommunity);
        when(optionalCommunity.orElse(any())).thenReturn(community);
        assertEquals("1", communityService.getById(1).getName());
    }

    @Test
    void getCount() {
        when(communityRepository.countByNameContainingOrDescriptionContaining(anyString(), anyString())).thenReturn(5);
        assertEquals(5, communityService.getCount("test"));
    }

    @Test
    void createCommunity() {
        Community community = new Community();
        community.setName("1");
        community.setDescription("1");
        communityService.createCommunity(community);
        verify(communityRepository).save(community);
    }

    @Test
    void setPhoto() {
        Community community = mock(Community.class);
        Optional<Community> optionalCommunity = mock(Optional.class);
        when(optionalCommunity.orElse(any())).thenReturn(community);
        when(communityRepository.findById(anyInt())).thenReturn(optionalCommunity);
        communityService.setPhoto(new byte[]{1}, 123);
        verify(community).setPhoto(any());
    }

    @Test
    void getPhoto() {
        Blob blob = mock(Blob.class);
        Community community = mock(Community.class);
        Optional<Community> optionalCommunity = mock(Optional.class);
        when(communityRepository.findById(anyInt())).thenReturn(optionalCommunity);
        when(optionalCommunity.orElse(any())).thenReturn(community);
        when(community.getPhoto()).thenReturn(blob);
        assertEquals(blob, communityService.getPhoto(2134));
    }

    @Test
    void deleteCommunity() {
        Optional<Community> optionalCommunity = mock(Optional.class);
        Community community = new Community();
        when(communityRepository.findById(anyInt())).thenReturn(optionalCommunity);
        when(optionalCommunity.orElse(any())).thenReturn(community);
        communityService.deleteCommunity(123);
        verify(communityRepository).save(community);
        verify(communityRepository).deleteById(123);
    }

    @Test
    void updateCommunity() {
        Community community = new Community();
        community.setName("1");
        community.setDescription("1");
        communityService.updateCommunity(1, community);
        community.setId(1);
        verify(communityRepository).save(community);
    }

    @Test
    void getCommunities() {
        List<Community> communityList = new ArrayList<>();
        communityList.add(new Community());
        when(communityRepository.findAll()).thenReturn(communityList);
        assertEquals(1, communityService.getCommunities().size());
    }

    @Test
    void getCommunity() {
        Optional<Community> optionalCommunity = mock(Optional.class);
        Community community = new Community();
        community.setName("1");
        when(communityRepository.findById(anyInt())).thenReturn(optionalCommunity);
        when(optionalCommunity.orElse(any())).thenReturn(community);
        assertEquals("1", communityService.getCommunity(123).getName());
    }

    @Test
    void getCommunityLike() {
        List<Community> communities = new ArrayList<>();
        Community community = new Community();
        community.setId(1);
        community.setName("1");
        community.setDescription("1");
        community.setCreatorId(1);
        communities.add(community);
        when(communityRepository.getByNameContainingOrDescriptionContaining(anyString(), anyString(), any()))
                .thenReturn(communities);
        assertEquals(1, communityService.getCommunityLike("test", 1).size());
    }

}
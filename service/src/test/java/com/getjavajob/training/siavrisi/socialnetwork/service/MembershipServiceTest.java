package com.getjavajob.training.siavrisi.socialnetwork.service;

import com.getjavajob.training.siavrisi.socialnetwork.domain.Account;
import com.getjavajob.training.siavrisi.socialnetwork.domain.Community;
import com.getjavajob.training.siavrisi.socialnetwork.repository.AccountRepository;
import com.getjavajob.training.siavrisi.socialnetwork.repository.CommunityRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MembershipServiceTest {

    @Mock
    private AccountRepository accountRepository;

    @Mock
    private CommunityRepository communityRepository;

    private MembershipService membershipService;

    @BeforeEach
    void setUp() {
        membershipService = new MembershipService(accountRepository, communityRepository);
    }

    @Test
    void getAccountCommunities() {
        Optional<Account> optionalAccount = mock(Optional.class);
        Account account = new Account();
        account.setFirstName("1");
        account.setLastName("1");
        Community community = new Community();
        community.setName("1");
        community.setId(1);
        community.setDescription("1");
        account.getCommunities().add(community);
        when(optionalAccount.orElse(any())).thenReturn(account);
        when(accountRepository.findById(anyInt())).thenReturn(optionalAccount);
        assertEquals("1", membershipService.getAccountCommunities(2342341).get(0).getName());
    }

    @Test
    void getCommunityAccounts() {
        Optional<Community> optionalCommunity = mock(Optional.class);
        Community community = new Community();
        community.setName("1");
        community.setDescription("1");
        Account account = new Account();
        account.setId(1);
        account.setFirstName("1");
        account.setLastName("1");
        account.setPatronymic("1");
        community.getAccounts().add(account);
        when(optionalCommunity.orElse(any())).thenReturn(community);
        when(communityRepository.findById(anyInt())).thenReturn(optionalCommunity);
        assertEquals("1", membershipService.getCommunityAccounts(23423).get(0).getFirstName());
    }

    @Test
    void checkMember() {
        Optional<Community> optionalCommunity = mock(Optional.class);
        Optional<Account> optionalAccount = mock(Optional.class);
        Community community = new Community();
        community.setName("1");
        community.setDescription("1");
        Account account = new Account();
        account.setId(1);
        account.setFirstName("1");
        account.setLastName("1");
        account.setPatronymic("1");
        community.getAccounts().add(account);
        when(accountRepository.findById(anyInt())).thenReturn(optionalAccount);
        when(communityRepository.findById(anyInt())).thenReturn(optionalCommunity);
        when(optionalAccount.orElse(any())).thenReturn(account);
        when(optionalCommunity.orElse(any())).thenReturn(community);
        assertTrue(membershipService.checkMember(999, 111));
    }

    @Test
    void addMember() {
        Optional<Community> optionalCommunity = mock(Optional.class);
        Optional<Account> optionalAccount = mock(Optional.class);
        Community community = new Community();
        community.setName("1");
        community.setDescription("1");
        Account account = new Account();
        account.setId(1);
        account.setFirstName("1");
        account.setLastName("1");
        account.setPatronymic("1");
        community.getAccounts().add(account);
        account.getCommunities().add(community);
        when(accountRepository.findById(anyInt())).thenReturn(optionalAccount);
        when(communityRepository.findById(anyInt())).thenReturn(optionalCommunity);
        when(optionalAccount.orElse(any())).thenReturn(account);
        when(optionalCommunity.orElse(any())).thenReturn(community);
        membershipService.addMember(223, 2312);
        verify(communityRepository).save(any());
    }

    @Test
    void deleteMember() {
        Optional<Community> optionalCommunity = mock(Optional.class);
        Optional<Account> optionalAccount = mock(Optional.class);
        Community community = new Community();
        community.setName("1");
        community.setDescription("1");
        Account account = new Account();
        account.setId(1);
        account.setFirstName("1");
        account.setLastName("1");
        account.setPatronymic("1");
        community.getAccounts().add(account);
        account.getCommunities().add(community);
        when(accountRepository.findById(anyInt())).thenReturn(optionalAccount);
        when(communityRepository.findById(anyInt())).thenReturn(optionalCommunity);
        when(optionalAccount.orElse(any())).thenReturn(account);
        when(optionalCommunity.orElse(any())).thenReturn(community);
        membershipService.deleteMember(223, 2312);
        verify(communityRepository).save(any());
    }

}
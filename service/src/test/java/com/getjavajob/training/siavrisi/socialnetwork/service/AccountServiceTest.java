package com.getjavajob.training.siavrisi.socialnetwork.service;

import com.getjavajob.training.siavrisi.socialnetwork.domain.Account;
import com.getjavajob.training.siavrisi.socialnetwork.domain.Friendship;
import com.getjavajob.training.siavrisi.socialnetwork.domain.Phone;
import com.getjavajob.training.siavrisi.socialnetwork.dto.AccountDTO;
import com.getjavajob.training.siavrisi.socialnetwork.repository.AccountRepository;
import com.getjavajob.training.siavrisi.socialnetwork.repository.FriendshipRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Blob;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AccountServiceTest {

    @Mock
    private AccountRepository accountRepository;

    @Mock
    private FriendshipRepository friendshipRepository;

    private AccountService accountService;

    @BeforeEach
    void setUp() {
        this.accountService = new AccountService(accountRepository, friendshipRepository);
    }

    @Test
    void createAccount() {
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setFirstName("1");
        accountDTO.setLastName("1");
        accountDTO.setBirthday(new Date(System.currentTimeMillis()));
        accountDTO.setPassword("1");
        accountDTO.setEmail("1");
        accountDTO.setSkype("1");
        accountDTO.setIcq("1");
        assertTrue(accountService.createAccount(accountDTO, ""));
        verify(accountRepository).save(any());
    }

    @Test
    void createAccountException() {
        AccountDTO accountDTO = new AccountDTO();
        assertThrows(NullPointerException.class, () -> {
            accountService.createAccount(accountDTO, "");
        });
    }

    @Test
    void addFriend() {
        Optional<Account> optionalAccount = mock(Optional.class);
        Account account = new Account();
        when(accountRepository.findById(anyInt())).thenReturn(optionalAccount);
        when(optionalAccount.orElse(any())).thenReturn(account);
        accountService.addFriend(1, 2);
        verify(friendshipRepository).save(any());
    }

    @Test
    void deleteFriend() {
        accountService.deleteFriend(234, 324);
        verify(friendshipRepository).existsById(any());
        verify(friendshipRepository).deleteById(any());
    }

    @Test
    void acceptFriend() {
        Optional<Friendship> optionalFriendship = mock(Optional.class);
        Friendship friendship = new Friendship();
        when(friendshipRepository.findById(any())).thenReturn(optionalFriendship);
        when(optionalFriendship.isPresent()).thenReturn(true);
        when(optionalFriendship.get()).thenReturn(friendship);
        accountService.acceptFriend(34, 2);
        verify(friendshipRepository).save(any());
    }

    @Test
    void getUnacceptedFriendsFrom() {
        List<Friendship> friendships = new ArrayList<>();
        Account account = new Account();
        account.setId(1);
        account.setFirstName("1");
        account.setLastName("1");
        account.setPatronymic("1");
        friendships.add(new Friendship());
        friendships.get(0).setAccountTo(account);
        when(friendshipRepository.findByKeyAccountFromIdAndAcceptedEquals(anyInt(), anyBoolean()))
                .thenReturn(friendships);
        assertEquals(1, accountService.getUnacceptedFriendsFrom(423).size());
    }

    @Test
    void getUnacceptedFriendsTo() {
        List<Friendship> friendships = new ArrayList<>();
        when(friendshipRepository.findByKeyAccountToIdAndAcceptedEquals(anyInt(), anyBoolean()))
                .thenReturn(friendships);
        Account account = new Account();
        account.setId(1);
        account.setFirstName("1");
        account.setLastName("1");
        account.setPatronymic("1");
        friendships.add(new Friendship());
        friendships.get(0).setAccountFrom(account);
        assertEquals(1, accountService.getUnacceptedFriendsTo(423).size());
    }

    @Test
    void getAcceptedFriends() {
        List<Friendship> friendships = new ArrayList<>();
        when(friendshipRepository.findByKeyAccountFromIdAndAcceptedEquals(anyInt(), anyBoolean()))
                .thenReturn(friendships);
        Account account = new Account();
        account.setId(1);
        account.setFirstName("1");
        account.setLastName("1");
        account.setPatronymic("1");
        friendships.add(new Friendship());
        friendships.get(0).setAccountFrom(account);
        friendships.get(0).setAccountTo(account);
        assertEquals(1, accountService.getUnacceptedFriendsFrom(423).size());
    }

    @Test
    void isFriends() {
        Optional<Friendship> friendship = mock(Optional.class);
        when(friendshipRepository.findById(any())).thenReturn(friendship);
        when(friendship.isPresent()).thenReturn(true);
        assertTrue(accountService.isFriends(3, 3));
    }

    @Test
    void updateAccount() {
        Account account = new Account();
        account.setFirstName("1");
        account.setLastName("1");
        account.setEmail("1");
        account.setBirthday(new Date(System.currentTimeMillis()));
        account.setPassword("1");
        account.setSkype("");
        account.setIcq("");
        accountService.updateAccount(1, account, new String[]{}, new String[]{});
        verify(accountRepository).save(any());
    }

    @Test
    void deleteAccount() {
        accountService.deleteAccount(23);
        verify(accountRepository).deleteById(anyInt());
    }

    @Test
    void getAccountById() {
        Optional<Account> optionalAccount = mock(Optional.class);
        Account account = new Account();
        account.setFirstName("1");
        account.setLastName("1");
        account.setPatronymic("1");
        account.setSkype("1");
        account.setIcq("1");
        account.setId(1);
        account.setEmail("1");
        account.setHomeAddress("1");
        account.setWorkAddress("1");
        account.setExtra("1");
        when(accountRepository.findById(anyInt())).thenReturn(optionalAccount);
        when(optionalAccount.orElse(any())).thenReturn(account);
        assertEquals("1", accountService.getAccountById(1).getFirstName());
    }

    @Test
    void getById() {
        Optional<Account> optionalAccount = mock(Optional.class);
        Account account = new Account();
        account.setFirstName("1");
        when(accountRepository.findById(anyInt())).thenReturn(optionalAccount);
        when(optionalAccount.orElse(any())).thenReturn(account);
        assertEquals("1", accountService.getById(123).getFirstName());
    }

    @Test
    void getAccountByEmail() {
        Account account = new Account();
        account.setFirstName("1");
        when(accountRepository.getByEmailEquals(anyString())).thenReturn(account);
        assertEquals("1", accountService.getAccountByEmail("1324").getFirstName());
    }

    @Test
    void getCount() {
        when(accountRepository.countByFirstNameContainingOrLastNameContainingOrPatronymicContaining(anyString(),
                anyString(), anyString())).thenReturn(5);
        assertEquals(5, accountService.getCount("test"));
    }

    @Test
    void getAccounts() {
        List<Account> accounts = new ArrayList<>();
        Account account = new Account();
        accounts.add(account);
        when(accountRepository.findAll()).thenReturn(accounts);
        assertEquals(1, accountService.getAccounts().size());
    }

    @Test
    void getPhones() {
        Optional<Account> optionalAccount = mock(Optional.class);
        Account account = new Account();
        Set<Phone> phones = new HashSet<>();
        phones.add(new Phone("3423"));
        account.setPhones(phones);
        when(accountRepository.findById(anyInt())).thenReturn(optionalAccount);
        when(optionalAccount.orElse(any())).thenReturn(account);
        assertEquals(1, accountService.getPhones(3).size());
    }

    @Test
    void getAccountsLike() {
        List<Account> accounts = new ArrayList<>();
        Account account = new Account();
        account.setId(1);
        account.setFirstName("1");
        account.setLastName("1");
        account.setPatronymic("1");
        accounts.add(account);
        when(accountRepository.getByFirstNameContainingOrLastNameContainingOrPatronymicContaining(anyString(),
                anyString(), anyString(), any())).thenReturn(accounts);
        assertEquals(1, accountService.getAccountsLike("test", 919919).size());
    }

    @Test
    void setPhoto() {
        Account account = mock(Account.class);
        Optional<Account> optionalAccount = mock(Optional.class);
        when(accountRepository.findById(anyInt())).thenReturn(optionalAccount);
        when(optionalAccount.orElse(any())).thenReturn(account);
        accountService.setPhoto(new byte[]{1}, 919);
        verify(account).setPhoto(any());
    }

    @Test
    void getPhoto() {
        Blob blob = mock(Blob.class);
        Account account = mock(Account.class);
        Optional<Account> optionalAccount = mock(Optional.class);
        when(accountRepository.findById(anyInt())).thenReturn(optionalAccount);
        when(optionalAccount.orElse(any())).thenReturn(account);
        when(account.getPhoto()).thenReturn(blob);
        assertEquals(blob, accountService.getPhoto(12));
    }

    @Test
    void validatePhoneNumber() {
        String phone = "+71234567890";
        List<Account> accounts = new ArrayList<>();
        when(accountRepository.findAll()).thenReturn(accounts);
        assertTrue(accountService.validatePhoneNumber(phone));
        assertFalse(accountService.validatePhoneNumber(""));
        assertThrows(NumberFormatException.class, () -> {
            accountService.validatePhoneNumber("883883");
        });
    }

}
package com.getjavajob.training.siavrisi.socialnetwork.config;

import com.getjavajob.training.siavrisi.socialnetwork.converter.StringToDateConverter;
import org.apache.catalina.Context;
import org.apache.catalina.startup.Tomcat;
import org.apache.tomcat.util.descriptor.web.ContextResource;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.embedded.tomcat.TomcatWebServer;
import org.springframework.boot.web.servlet.server.ServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.format.FormatterRegistry;
import org.springframework.jndi.JndiObjectFactoryBean;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.mvc.WebContentInterceptor;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import javax.naming.NamingException;
import javax.sql.DataSource;

@Configuration
@EnableWebMvc
@Import(ServiceConfig.class)
public class WebConfiguration implements WebMvcConfigurer {

    @Bean
    public ServletWebServerFactory webServerFactory() {
        return new TomcatServletWebServerFactory() {

            @Override
            protected TomcatWebServer getTomcatWebServer(Tomcat tomcat) {
                System.setProperty("catalina.useNaming", "true");
                tomcat.enableNaming();
                return new TomcatWebServer(tomcat, this.getPort() >= 0, this.getShutdown());
            }

            @Override
            protected void postProcessContext(Context context) {
                ContextResource contextResource = new ContextResource();
                contextResource.setType(DataSource.class.getName());
                contextResource.setName("jdbc/socialnetwork");
                contextResource.setProperty("factory", "com.zaxxer.hikari.HikariJNDIFactory");
                contextResource.setProperty("driverClassName", "com.mysql.cj.jdbc.Driver");
                contextResource.setProperty("jdbcUrl", "jdbc:mysql://us-cdbr-east-04.cleardb" +
                        ".com:3306/heroku_3e3ebb3d5c1bfd3?reconnect=true&characterEncoding=utf8");
                contextResource.setProperty("username", "b163c9403b06bd");
                contextResource.setProperty("password", "c56abc63");
                contextResource.setProperty("maxTotal", "8");
                contextResource.setProperty("maxWaitMillis", "-1");
                context.getNamingResources().addResource(contextResource);
            }

        };

    }

    @Bean
    public ViewResolver viewResolver() {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/jsp/");
        viewResolver.setSuffix(".jsp");
        return viewResolver;
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("login");
        registry.addViewController("/login").setViewName("login");
        registry.addViewController("/searchAjax").setViewName("searchAjax");
        registry.addViewController("/index").setViewName("index");
    }

    @Override
    public void addResourceHandlers(final ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static-resources/**").addResourceLocations("/static-resources/");
    }

    @Bean
    WebContentInterceptor webContentInterceptor() {
        WebContentInterceptor webContentInterceptor = new WebContentInterceptor();
        webContentInterceptor.setCacheSeconds(60);
        return webContentInterceptor;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(webContentInterceptor());
    }

    @Bean
    public CommonsMultipartResolver multipartResolver() {
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
        multipartResolver.setMaxUploadSize(100000000);
        return multipartResolver;
    }

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(new StringToDateConverter());
    }

    @Bean
    public DataSource jndiDataSource() throws IllegalArgumentException, NamingException {
        JndiObjectFactoryBean bean = new JndiObjectFactoryBean();
        bean.setJndiName("java:/comp/env/jdbc/socialnetwork");
        bean.setProxyInterface(DataSource.class);
        bean.setLookupOnStartup(false);
        bean.afterPropertiesSet();
        return (DataSource) bean.getObject();
    }

}

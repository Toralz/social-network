package com.getjavajob.training.siavrisi.socialnetwork.controller;

import com.getjavajob.training.siavrisi.socialnetwork.model.ChatMessage;
import com.getjavajob.training.siavrisi.socialnetwork.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.servlet.ModelAndView;

import java.util.Objects;

@Controller
@ComponentScan(basePackages = "com.getjavajob.training.siavrisi.socialnetwork.service")
public class WebSocketController {

    private final AccountService accountService;

    @Autowired
    public WebSocketController(AccountService accountService) {
        this.accountService = accountService;
    }

    @RequestMapping("/chat/{chatId}")
    public ModelAndView getChat(@SessionAttribute(name = "userID") int userId,
                                @PathVariable(name = "chatId") String chatId) {
        ModelAndView modelAndView = new ModelAndView("chat");
        modelAndView.addObject("chatId", chatId);
        modelAndView.addObject("firstName", accountService.getAccountById(userId).getFirstName());
        return modelAndView;
    }

    @MessageMapping("/chat/sendMessage/{chatId}")
    @SendTo("/topic/{chatId}")
    public ChatMessage sendMessage(@Payload ChatMessage chatMessage) {
        return chatMessage;
    }

    @MessageMapping("/chat/addUser/{chatId}")
    @SendTo("/topic/{chatId}")
    public ChatMessage addUser(@Payload ChatMessage chatMessage,
                               SimpMessageHeaderAccessor headerAccessor) {
        Objects.requireNonNull(headerAccessor.getSessionAttributes()).put("userID", chatMessage.getSenderId());
        return chatMessage;
    }

}

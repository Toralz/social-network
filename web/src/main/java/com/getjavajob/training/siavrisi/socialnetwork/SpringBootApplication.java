package com.getjavajob.training.siavrisi.socialnetwork;

import com.getjavajob.training.siavrisi.socialnetwork.config.SecurityConfiguration;
import com.getjavajob.training.siavrisi.socialnetwork.config.WebConfiguration;
import com.getjavajob.training.siavrisi.socialnetwork.config.WebSocketConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Import;

@org.springframework.boot.autoconfigure.SpringBootApplication
@Import({WebConfiguration.class, SecurityConfiguration.class, WebSocketConfig.class})
public class SpringBootApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder applicationBuilder) {
        return applicationBuilder.sources(SpringBootApplication.class);
    }

}
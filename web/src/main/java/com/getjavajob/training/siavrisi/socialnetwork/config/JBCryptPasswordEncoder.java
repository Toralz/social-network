package com.getjavajob.training.siavrisi.socialnetwork.config;

import org.springframework.security.crypto.password.PasswordEncoder;

import static org.mindrot.jbcrypt.BCrypt.checkpw;
import static org.mindrot.jbcrypt.BCrypt.gensalt;
import static org.mindrot.jbcrypt.BCrypt.hashpw;

/**
 * Custom password encoder based on JBCrypt password encoder to be used in security configuration
 */
public class JBCryptPasswordEncoder implements PasswordEncoder {

    @Override
    public String encode(CharSequence rawPassword) {
        return hashpw(rawPassword.toString(), gensalt());
    }

    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        return checkpw(rawPassword.toString(), encodedPassword);
    }

}

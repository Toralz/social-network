package com.getjavajob.training.siavrisi.socialnetwork.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.NonNull;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class StringToDateConverter implements Converter<String, Date> {

    @Override
    public Date convert(@NonNull String string) {
        try {
            return new Date(new SimpleDateFormat("MM/dd/yyyy").parse(string).getTime());
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

}

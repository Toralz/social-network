package com.getjavajob.training.siavrisi.socialnetwork.controller;

import com.getjavajob.training.siavrisi.socialnetwork.domain.Community;
import com.getjavajob.training.siavrisi.socialnetwork.dto.CommunityDTO;
import com.getjavajob.training.siavrisi.socialnetwork.service.CommunityService;
import com.getjavajob.training.siavrisi.socialnetwork.service.MembershipService;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.annotation.MultipartConfig;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.List;

/**
 * Controller to work with communities
 */
@MultipartConfig
@Controller
@RequestMapping(value = "/community")
@ComponentScan(basePackages = "com.getjavajob.training.siavrisi.socialnetwork.service")
public class CommunityController {

    private static final Logger logger = LoggerFactory.getLogger(CommunityController.class);
    private static final String REDIRECT_VIEW = "redirect:/community/";
    private static final String USERID = "userID";

    private final CommunityService communityService;

    private final MembershipService membershipService;

    @Autowired
    public CommunityController(CommunityService communityService, MembershipService membershipService) {
        this.communityService = communityService;
        this.membershipService = membershipService;
    }

    @PostMapping(value = "/member/{communityId}/{userId}")
    public boolean addMember(@PathVariable("communityId") int communityId, @PathVariable("userId") int userId) {
        logger.info("Adding member {} for {} community", userId, communityId);
        membershipService.addMember(userId, communityId);
        return true;
    }

    @RequestMapping(value = "/membersList")
    public ModelAndView getMembersList(@RequestParam("communityId") int communityId,
                                       @SessionAttribute(USERID) int userId) {
        logger.info("Getting members list for {}", communityId);
        ModelAndView modelAndView = new ModelAndView("membersList");
        boolean isCreator = communityService.checkCreator(communityId, userId);
        modelAndView.addObject("isCreator", isCreator);
        modelAndView.addObject("accounts", membershipService.getCommunityAccounts(communityId));
        return modelAndView;
    }

    @GetMapping(value = "/{id}")
    public ModelAndView viewCommunity(@PathVariable("id") int communityId, @SessionAttribute(USERID) int userId) {
        logger.info("Showing {} community", communityId);
        ModelAndView modelAndView = new ModelAndView("viewCommunity");
        Community community = communityService.getCommunity(communityId);
        boolean isMember = membershipService.checkMember(userId, communityId);
        logger.info("Checking if {} is a member of {} community", userId, communityId);
        boolean isCreator = communityService.checkCreator(communityId, userId);
        logger.info("Checking if {} is a creator of {} community", userId, communityId);
        modelAndView.addObject("isMember", isMember);
        modelAndView.addObject("isCreator", isCreator);
        modelAndView.addObject("community", community);
        return modelAndView;
    }

    @GetMapping(value = "/communityCreation")
    public ModelAndView communityCreation() {
        logger.info("Showing community creation page");
        ModelAndView modelAndView = new ModelAndView("createCommunity");
        modelAndView.addObject("community", new CommunityDTO());
        return modelAndView;
    }

    @PostMapping(value = "/createCommunity")
    public String createCommunity(@ModelAttribute("community") CommunityDTO communityDTO,
                                  @SessionAttribute(USERID) int userId) {
        Community community = new Community(communityDTO);
        logger.info("Community creation for {} by {} user", community, userId);
        community.setCreatorId(userId);
        if (communityService.createCommunity(community)) {
            logger.info("Successfully created community");
            int communityId = communityService.getByName(community.getName()).getId();
            membershipService.addMember(userId, communityId);
            return REDIRECT_VIEW + communityId;
        } else {
            logger.error("Error occurred during community creation");
            return "createCommunity";
        }
    }

    @GetMapping(value = "/photo/{id}")
    @ResponseBody
    public byte[] displayCommunityPhoto(@PathVariable("id") int id) {
        logger.info("Displaying {} community photo", id);
        Blob photo = communityService.getPhoto(id);
        if (photo != null) {
            try {
                return IOUtils.toByteArray(photo.getBinaryStream());
            } catch (SQLException | IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
                return IOUtils.toByteArray(new FileInputStream("src/main/webapp/static-resources/default_photo" +
                        ".png"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @GetMapping(value = "/communityPhoto")
    public ModelAndView uploadCommunityPhoto(@RequestParam("communityId") int communityId,
                                             @SessionAttribute(USERID) int userId) {
        logger.info("Showing community photo upload for {} community by {} user", communityId, userId);
        boolean isCreator = communityService.checkCreator(communityId, userId);
        Community community = communityService.getCommunity(communityId);
        ModelAndView modelAndView = new ModelAndView("communityPhoto");
        modelAndView.addObject("isCreator", isCreator);
        modelAndView.addObject("community", community);
        return modelAndView;
    }

    @PostMapping(value = "/photo/{id}", consumes = "multipart/form-data")
    public String setCommunityPhoto(@PathVariable("id") int id, @RequestPart("photo") MultipartFile photo)
            throws IOException {
        if (communityService.setPhoto(photo.getBytes(), id)) {
            logger.info("Set photo successfully");
            return REDIRECT_VIEW + id;
        } else {
            logger.error("Error during setting photo");
            return "communityPhoto" + id;
        }
    }

    @DeleteMapping(value = "/member/{communityId}/{userId}")
    public boolean removeMember(@PathVariable("userId") int userId, @PathVariable("communityId") int communityId) {
        logger.info("Removing {} member from {} community", userId, communityId);
        membershipService.deleteMember(userId, communityId);
        return true;
    }

    @DeleteMapping(value = "/{id}")
    public void deleteCommunity(@PathVariable("id") int communityId, @SessionAttribute(USERID) int userId) {
        logger.info("Deleting {} community", communityId);
        if (communityService.checkCreator(communityId, userId)) {
            communityService.deleteCommunity(communityId);
            logger.info("Deleted {} community successfully by {} user", communityId, userId);
        } else {
            logger.error("{} user is not a creator of {} community", userId, communityId);
        }
    }

    @GetMapping(value = "/communityList")
    public ModelAndView getCommunityList(@RequestParam("id") int userId) {
        logger.info("Showing community list for {} user", userId);
        ModelAndView modelAndView = new ModelAndView("communityList");
        List<CommunityDTO> communities = membershipService.getAccountCommunities(userId);
        modelAndView.addObject("communities", communities);
        return modelAndView;
    }

}

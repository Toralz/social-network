package com.getjavajob.training.siavrisi.socialnetwork.controller;

import com.getjavajob.training.siavrisi.socialnetwork.dto.AccountDTO;
import com.getjavajob.training.siavrisi.socialnetwork.dto.CommunityDTO;
import com.getjavajob.training.siavrisi.socialnetwork.service.AccountService;
import com.getjavajob.training.siavrisi.socialnetwork.service.CommunityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Controller to work with search requests
 */
@Controller
@ComponentScan(basePackages = "com.getjavajob.training.siavrisi.socialnetwork.service")
public class SearchController {

    private static final int ACCOUNT = 1;
    private static final int COMMUNITY = 2;

    private final AccountService accountService;

    private final CommunityService communityService;

    @Autowired
    public SearchController(AccountService accountService, CommunityService communityService) {
        this.accountService = accountService;
        this.communityService = communityService;
    }

    /**
     * Method to handle search request
     *
     * @param item        to be searched
     * @param searchInput to be searched like
     * @param page        to be displayed at
     * @param <T>         {@link AccountDTO} or
     *                    {@link CommunityDTO}
     * @return list of DTO objects
     */
    @RequestMapping("/search")
    @ResponseBody
    public <T> List<T> search(@RequestParam("item") int item,
                              @RequestParam(required = false, name = "searchInput") String searchInput,
                              @RequestParam("page") int page) {
        if (item == ACCOUNT) {
            return (List<T>) accountService.getAccountsLike(searchInput, page);
        } else if (item == COMMUNITY) {
            return (List<T>) communityService.getCommunityLike(searchInput, page);
        }
        return null;
    }

    /**
     * Method to handle searchCount request
     *
     * @param item        to be searched
     * @param searchInput to be searched like
     * @return count of records in database like given {@code searchInput}
     */
    @RequestMapping("/searchCount")
    @ResponseBody
    public int searchCount(@RequestParam("item") int item,
                           @RequestParam(required = false, name = "searchInput") String searchInput) {
        if (item == ACCOUNT) {
            return accountService.getCount(searchInput);
        } else if (item == COMMUNITY) {
            return communityService.getCount(searchInput);
        }
        return 0;
    }

}

package com.getjavajob.training.siavrisi.socialnetwork.config;

import com.getjavajob.training.siavrisi.socialnetwork.model.AccountDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private static final Logger logger = LoggerFactory.getLogger(SecurityConfiguration.class);

    private final UserDetailsService accountDetailsService;

    @Autowired
    public SecurityConfiguration(UserDetailsService accountDetailsService) {
        this.accountDetailsService = accountDetailsService;
    }

    @Override
    protected void configure(final AuthenticationManagerBuilder builder) throws Exception {
        logger.info("In configure builder");
        builder.userDetailsService(accountDetailsService)
                .passwordEncoder(passwordEncoder());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new JBCryptPasswordEncoder();
    }

    @Override
    protected void configure(final HttpSecurity httpSecurity) throws Exception {
        logger.info("In configure httpSecurity");
        httpSecurity.csrf().disable()
                .authorizeRequests()
                .antMatchers("/account/registration").permitAll()
                .antMatchers("/account/registrationValidation").permitAll()
                .antMatchers("/login").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .usernameParameter("email")
                .passwordParameter("password")
                .successHandler((httpServletRequest, httpServletResponse, authentication) -> {
                    logger.info("Getting principal");
                    AccountDetails accountDetails = (AccountDetails) authentication.getPrincipal();
                    logger.info("Getting userID");
                    httpServletRequest.getSession().setAttribute("userID", accountDetails.getUserId());
                    httpServletResponse.sendRedirect("/index");
                })
                .permitAll()
                .and()
                .logout()
                .logoutUrl("/account/logout")
                .deleteCookies("JSESSIONID")
                .permitAll()
                .logoutSuccessUrl("/login")
                .and()
                .rememberMe().key("key").rememberMeParameter("cookies")
                .authenticationSuccessHandler((httpServletRequest, httpServletResponse, authentication) -> {
                    logger.info("Getting principal");
                    AccountDetails accountDetails = (AccountDetails) authentication.getPrincipal();
                    logger.info("Getting userID");
                    httpServletRequest.getSession().setAttribute("userID", accountDetails.getUserId());
                    httpServletResponse.sendRedirect("/index");
                });
    }

    @Override
    public void configure(WebSecurity webSecurity) {
        webSecurity.ignoring()
                .antMatchers("/static-resources/**");
    }

}

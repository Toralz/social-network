package com.getjavajob.training.siavrisi.socialnetwork.controller;

import com.getjavajob.training.siavrisi.socialnetwork.domain.Account;
import com.getjavajob.training.siavrisi.socialnetwork.dto.AccountDTO;
import com.getjavajob.training.siavrisi.socialnetwork.service.AccountService;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.annotation.MultipartConfig;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.List;

/**
 * Controller to work with accounts
 */
@MultipartConfig
@Controller
@RequestMapping(value = "/account")
@ComponentScan(basePackages = "com.getjavajob.training.siavrisi.socialnetwork.service")
public class AccountController {

    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);
    private static final String REDIRECT_INDEX = "redirect:/index";
    private static final String USERID = "userID";
    private static final String EMAIL = "email";
    private static final String ACCOUNT = "account";

    private final JmsTemplate jmsTemplate;

    private final AccountService accountService;

    @Autowired
    public AccountController(JmsTemplate jmsTemplate, AccountService accountService) {
        this.jmsTemplate = jmsTemplate;
        this.accountService = accountService;
    }

    @PutMapping(value = "/friends/{id}")
    @ResponseBody
    public String acceptFriend(@SessionAttribute(USERID) int accountToId,
                               @PathVariable("id") int accountFromId) {
        accountService.acceptFriend(accountFromId, accountToId);
        return "Request accepted";
    }

    @PostMapping(value = "/friends/{id}")
    @ResponseBody
    public String addFriend(@SessionAttribute(USERID) int accountFromId, @PathVariable("id") int accountToId) {
        accountService.addFriend(accountFromId, accountToId);
        return "Friend added";
    }

    @DeleteMapping(value = "/friends/{id}")
    @ResponseBody
    public String deleteFriend(@SessionAttribute(USERID) int accountFromId,
                               @PathVariable("id") int accountToId) {
        accountService.deleteFriend(accountFromId, accountToId);
        return "Friend deleted";
    }

    @GetMapping(value = "/friends")
    public ModelAndView viewFriends(@SessionAttribute(USERID) int userId) {
        logger.debug("In viewFriends");
        ModelAndView modelAndView = new ModelAndView("viewFriends");
        List<AccountDTO> unacceptedFriendsFrom = accountService.getUnacceptedFriendsFrom(userId);
        List<AccountDTO> unacceptedFriendsTo = accountService.getUnacceptedFriendsTo(userId);
        List<AccountDTO> acceptedFriends = accountService.getAcceptedFriends(userId);
        modelAndView.addObject("unacceptedFrom", unacceptedFriendsFrom);
        modelAndView.addObject("unacceptedTo", unacceptedFriendsTo);
        modelAndView.addObject("accepted", acceptedFriends);
        return modelAndView;
    }

    @GetMapping(value = "/{id}")
    public ModelAndView viewAccount(@SessionAttribute(name = USERID) int userId, @PathVariable("id") int id) {
        AccountDTO account = accountService.getAccountById(id);
        jmsTemplate.convertAndSend("Email",
                account.getEmail() + "\nYour page is getting viewed by " + userId + " user");
        logger.info("View account information for {} id", id);
        ModelAndView modelAndView = new ModelAndView("viewAccount");
        modelAndView.addObject("phones", account.getPhones());
        modelAndView.addObject(ACCOUNT, account);
        String chatId = "";
        boolean isFriends = accountService.isFriends(userId, id);
        if (userId < id) {
            chatId = userId + "w" + id;
        } else if (userId > id) {
            chatId = id + "w" + userId;
        } else {
            isFriends = true;
        }
        modelAndView.addObject("isFriends", isFriends);
        modelAndView.addObject("chatId", chatId);
        return modelAndView;
    }

    @GetMapping(value = "/welcome")
    public ModelAndView welcomeUser(@SessionAttribute(USERID) int userId) {
        logger.info("Placing welcome message for {} userId", userId);
        ModelAndView modelAndView = new ModelAndView("welcome");
        modelAndView.addObject(EMAIL, accountService.getAccountById(userId).getEmail());
        return modelAndView;
    }

    @GetMapping(value = "/photo/{id}", produces = MediaType.IMAGE_GIF_VALUE)
    @ResponseBody
    public byte[] displayUserPhoto(@PathVariable("id") int id) {
        logger.info("Displaying user photo for {} id", id);
        Blob photo = accountService.getPhoto(id);
        if (photo != null) {
            try {
                return IOUtils.toByteArray(photo.getBinaryStream());
            } catch (SQLException | IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
                return IOUtils.toByteArray(new FileInputStream("src/main/webapp/static-resources/default_photo" +
                        ".png"));
            } catch (IOException e) {
                try {
                    return IOUtils.toByteArray(new FileInputStream("target/web-1.0-SNAPSHOT/static-resources" +
                            "/default_photo.png"));
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
        return null;
    }

    @PostMapping(value = "/photo/{id}", consumes = "multipart/form-data")
    public String setPhoto(@PathVariable("id") int userId, @RequestPart("photo") MultipartFile photo)
            throws IOException {
        logger.info("Setting photo for {} id", userId);
        if (! accountService.setPhoto(photo.getBytes(), userId)) {
            logger.error("Error setting photo for {} id", userId);
            return "uploadPhoto";
        } else {
            logger.info("Successfully set photo for {} id", userId);
            return REDIRECT_INDEX;
        }
    }

    @GetMapping(value = "/getXml")
    public ResponseEntity<InputStreamResource> getFile(@SessionAttribute(USERID) int userId)
            throws FileNotFoundException {
        File file = accountService.getAccountXml(userId);
        InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + file.getName())
                .contentType(MediaType.APPLICATION_XML).contentLength(file.length()).body(resource);
    }

    @PostMapping(value = "/registrationValidation")
    public String registrationValidation(@ModelAttribute(ACCOUNT) AccountDTO accountDTO,
                                         @RequestParam("phone") String phone) {
        logger.info("Registration validation for {}", accountDTO);
        if (! accountService.createAccount(accountDTO, phone)) {
            return "registration";
        } else {
            return "login";
        }
    }

    @GetMapping(value = "/registration")
    public ModelAndView register() {
        logger.info("Requested registration page");
        ModelAndView modelAndView = new ModelAndView("registration");
        modelAndView.addObject(ACCOUNT, new AccountDTO());
        return modelAndView;
    }

    @PostMapping(value = "/updateFromXml", consumes = "multipart/form-data")
    public String updateFromXml(@SessionAttribute(USERID) int userId, @RequestPart("xml") MultipartFile xml)
            throws IOException {
        File file = new File(ACCOUNT + userId + ".xml");
        xml.transferTo(file);
        if (accountService.updateAccountFromXml(userId, file)) {
            return REDIRECT_INDEX;
        } else {
            return "editAccount";
        }
    }

    @GetMapping(value = "/updateFromXml")
    public ModelAndView getUpdateFromXml() {
        return new ModelAndView("updateFromXml");
    }

    @GetMapping(value = "/uploadPhoto")
    public ModelAndView getUploadPhoto() {
        return new ModelAndView("uploadPhoto");
    }

    @GetMapping(value = "/editAccount")
    public ModelAndView editAccount(@SessionAttribute(USERID) int userId) {
        logger.info("Account edition page for {} id", userId);
        AccountDTO account = accountService.getAccountById(userId);
        ModelAndView modelAndView = new ModelAndView("editAccount");
        modelAndView.addObject(ACCOUNT, account);
        return modelAndView;
    }

    @PostMapping(value = "/{id}")
    public String validateEdition(@PathVariable("id") int userId, @ModelAttribute("account") AccountDTO account,
                                  @RequestParam(required = false, name = "newPhones") String[] phonesToAdd,
                                  @RequestParam(required = false, name = "removedPhones") String[] removedPhones,
                                  @RequestParam(required = false, name = "phone") String[] newPhones) {
        logger.info("Edit validation for {} account, {} phonesToAdd, {} removedPhones, {} newPhones", account,
                phonesToAdd, removedPhones, newPhones);
        if (! accountService.updateAccount(userId, new Account(account), phonesToAdd, removedPhones)) {
            logger.error("Error occurred during edit validation for {}", userId);
            return "editAccount";
        } else {
            logger.info("Successfully edited account");
            return REDIRECT_INDEX;
        }
    }

}

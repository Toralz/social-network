<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--
  Created by IntelliJ IDEA.
  User: gyavr
  Date: 03.06.2021
  Time: 17:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static-resources/style.css"/>
</head>
<body>
<c:if test="${not empty userID}">
    <p><strong>Welcome ${email}<br/></strong></p>
</c:if>
</body>
</html>

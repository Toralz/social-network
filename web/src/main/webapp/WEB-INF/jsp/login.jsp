<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--
  Created by IntelliJ IDEA.
  User: gyavr
  Date: 31.05.2021
  Time: 20:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static-resources/style.css"/>
    <title>Login</title>
</head>
<body>
<sec:authorize access="isAuthenticated()">
    <jsp:forward page="index.jsp"/>
</sec:authorize>
<c:if test="${param.error != null}">
    <div id="error">
        <strong style="color: red">Invalid email or password</strong>
    </div>
</c:if>
<div class="loginForm">
    <form action="${pageContext.request.contextPath}/login" method="post">
        <table style="width: 50%">
            <tr>
                <td>Email</td>
                <td><input type="text" name="email" value=""/></td>
            </tr>
            <tr>
                <td>Password</td>
                <td><input type="password" name="password"/></td>
            </tr>
            <tr>
                <td>Use cookies</td>
                <td><input type="checkbox" name="cookies"/></td>
            </tr>
        </table>
        <input type="submit" value="Login"/>
    </form>
    <a href="${pageContext.request.contextPath}/account/registration">Sign up</a>
</div>

</body>
</html>

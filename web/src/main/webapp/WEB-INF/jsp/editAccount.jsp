<%@ page import="com.getjavajob.training.siavrisi.socialnetwork.service.AccountService" %>
<%@ page import="com.getjavajob.training.siavrisi.socialnetwork.domain.Account" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="input" uri="http://www.springframework.org/tags/form" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: gyavr
  Date: 01.06.2021
  Time: 20:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edit</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static-resources/style.css"/>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

</head>
<body>
<jsp:include page="included.jsp"/>
<h1>Edit form</h1>
<%--@elvariable id="account" type="com.getjavajob.training.siavrisi.socialnetwork.dto.AccountDTO"--%>
<form:form modelAttribute="account"
           action="${pageContext.request.contextPath}/account/${userID}" method="post"
           onsubmit="return confirm('Do you want to save the changes?');">
    <table>
        <tr>
            <td><form:label path="email">Change email</form:label></td>
            <td><form:input path="email" type="text" name="email" value="${account.email}"/></td>
        </tr>
        <tr>
            <td><form:label path="password">Change password</form:label></td>
            <td><form:input path="password" type="password" name="password" value=""/></td>
        </tr>
        <tr>
            <td><form:label path="firstName">Change first name</form:label></td>
            <td><form:input path="firstName" type="text" name="firstName" value="${account.firstName}"/></td>
        </tr>
        <tr>
            <td><form:label path="lastName">Change last name</form:label></td>
            <td><form:input path="lastName" type="text" name="lastName" value="${account.lastName}"/></td>
        </tr>
        <tr>
            <td><form:label path="patronymic">Change patronymic</form:label></td>
            <td><form:input path="patronymic" type="text" name="patronymic" value="${account.patronymic}"/></td>
        </tr>
        <tr>
            <td><form:label path="skype">Change skype</form:label></td>
            <td><form:input path="skype" type="text" name="skype" value="${account.skype}"/></td>
        </tr>
        <tr>
            <td><form:label path="icq">Change ICQ</form:label></td>
            <td><form:input path="icq" type="text" name="icq" value="${account.icq}"/></td>
        </tr>
        <tr>
            <td><form:label path="homeAddress">Change home address</form:label></td>
            <td><form:input path="homeAddress" type="text" name="homeAddress" value="${account.homeAddress}"/></td>
        </tr>
        <tr>
            <td><form:label path="workAddress">Change work address</form:label></td>
            <td><form:input path="workAddress" type="text" name="workAddress" value="${account.workAddress}"/></td>
        </tr>
        <tr>
            <td><form:label path="extra">Change extra information</form:label></td>
            <td><form:input path="extra" type="text" name="extra" value="${account.extra}"/></td>
        </tr>
        <tr>
            <td><form:label path="birthday">Change birthday</form:label></td>
            <td><form:input path="birthday" type="text" id="datepicker" name="birthday"
                            value="${account.birthday}"/></td>
        </tr>
    </table>
    <table id="phones" align="left" style="vertical-align: top;" border="1px">
        <c:forEach var="phone" items="${account.phones}">
            <tr>
                <td>${phone.phone}</td>
                <td>
                    <button onclick="deletePhone(this)">Delete phone</button>
                </td>
            </tr>
        </c:forEach>
    </table>
    <div id="removedPhones"></div>
    <p><input type="submit" value="Save changes" style="vertical-align: bottom"/></p>
</form:form>
<input type="text" id="newPhone" align="right" style="vertical-align: top;">
<button style="vertical-align: top" onclick="validatePhone()">Add phone</button>
<script src="../../static-resources/scripts.js"></script>
</body>
</html>

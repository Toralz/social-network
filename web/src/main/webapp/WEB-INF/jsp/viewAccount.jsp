<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="com.getjavajob.training.siavrisi.socialnetwork.service.AccountService" %>
<%@ page import="java.util.List" %>
<%@ page import="com.getjavajob.training.siavrisi.socialnetwork.domain.Account" %>
<%@ page import="java.util.Arrays" %><%--
  Created by IntelliJ IDEA.
  User: gyavr
  Date: 31.05.2021
  Time: 15:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Account page</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static-resources/style.css"/>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
            crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</head>
<body>
<jsp:include page="included.jsp"/>
<div id="accordion">
    <h3>Account information</h3>
    <div>
        <p>
            ${account.firstName}
            ${account.lastName}
            ${account.patronymic}
        <p><img width="300" height="300" src="${pageContext.request.contextPath}/account/photo/${account.id}"/>
        </p>
        <c:forEach var="phone" items="${phones}">
            <p>${phone}</p>
        </c:forEach>
        <c:if test="${!isFriends}">
            <input type="button" value="Add friend" id="${account.id}" class="addFriend"/>
            <strong id="success" style="color: green"></strong>
        </c:if>
        <c:if test="${userID != account.id}">
            <a href="<c:url value="/chat/${chatId}"/>" class="btn btn-secondary" role="button">Open chat</a>
        </c:if>
        </p>
    </div>
    <h3>List of communities</h3>
    <div>
        <p>
            <jsp:include page="/community/communityList?id=${account.id}"/>
        </p>
    </div>
</div>
<script src="../../static-resources/const.js"></script>
<script src="../../static-resources/scripts.js"></script>
<script src="../../static-resources/friend-scripts.js"></script>
</body>
</html>

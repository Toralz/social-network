<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--
  Created by IntelliJ IDEA.
  User: gyavr
  Date: 16.06.2021
  Time: 16:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static-resources/style.css"/>
    <title>Community list</title>
</head>
<body>
<table>
    <c:forEach var="community" items="${communities}">
        <tr>
            <td><a href="${pageContext.request.contextPath}/community/${community.id}">${community.name}</a></td>
            <td><a href="${pageContext.request.contextPath}/community/${community.id}">${community.description}</a></td>
        </tr>
    </c:forEach>
</table>

</body>
</html>

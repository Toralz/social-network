<%--
  Created by IntelliJ IDEA.
  User: gyavr
  Date: 01.06.2021
  Time: 18:20
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Registration</title>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static-resources/style.css"/>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
</head>
<body>
<%--@elvariable id="account" type="com.getjavajob.training.siavrisi.socialnetwork.domain.Account"--%>
<form:form action="${pageContext.request.contextPath}/account/registrationValidation" modelAttribute="account"
           method="post">
    <table style="width: 50%">
        <tr>
            <td><form:label path="email">Email<strong style = "color:red">*</strong></form:label></td>
            <td><form:input type="text" name="email" path="email"/></td>
        </tr>
        <tr>
            <td><form:label path="password">Password<strong style = "color:red">*</strong></form:label></td>
            <td><form:input type="password" name="password" path="password"/></td>
        </tr>
        <tr>
            <td><form:label path="firstName">First name<strong style = "color:red">*</strong></form:label></td>
            <td><form:input type="text" name="firstName" path="firstName"/></td>
        </tr>
        <tr>
            <td><form:label path="lastName">Last name<strong style = "color:red">*</strong></form:label></td>
            <td><form:input type="text" name="lastName" path="lastName"/></td>
        </tr>
        <tr>
            <td><form:label path="patronymic">Patronymic</form:label></td>
            <td><form:input type="text" name="patronymic" path="patronymic"/></td>
        </tr>
        <tr>
            <td><form:label path="skype">Skype</form:label></td>
            <td><form:input type="text" name="skype" path="skype"/></td>
        </tr>
        <tr>
            <td><form:label path="icq">ICQ</form:label></td>
            <td><form:input type="text" name="icq" path="icq"/></td>
        </tr>
        <tr>
            <td><form:label path="homeAddress">Home address</form:label></td>
            <td><form:input type="text" name="homeAddress" path="homeAddress"/></td>
        </tr>
        <tr>
            <td><form:label path="workAddress">Work address</form:label></td>
            <td><form:input type="text" name="workAddress" path="workAddress"/></td>
        </tr>
        <tr>
            <td><form:label path="extra">Some extra information</form:label></td>
            <td><form:input type="text" name="extra" path="extra"/></td>
        </tr>
        <tr>
            <td><form:label path="birthday">Day of birth</form:label></td>
            <td><form:input type="text" id="datepicker" name="birthday" path="birthday"/></td>
        </tr>
        <tr>
            <td><label>Phone</label></td>
            <td><input type="text" name="phone"/></td>
        </tr>
    </table>
    <input type="submit" value="Submit"/>
</form:form>
<script src="../../static-resources/scripts.js"></script>
</body>
</html>

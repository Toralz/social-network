<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--
  Created by IntelliJ IDEA.
  User: gyavr
  Date: 17.06.2021
  Time: 17:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static-resources/style.css"/>
    <title>List of members</title>
</head>
<body>
<table>
    <c:forEach var="account" items="${accounts}">
        <tr>
            <td><a href="${pageContext.request.contextPath}/account/${account.id}">${account.firstName}</a></td>
            <td><a href="${pageContext.request.contextPath}/account/${account.id}">${account.lastName}</a></td>
            <td><a href="${pageContext.request.contextPath}/account/${account.id}">${account.patronymic}</a></td>
            <c:if test="${isCreator}">
                <c:if test="${userID != account.id}">
                    <td>
                        <input type="button" class="deleteMember" userid="${account.id}"
                               communityid="${param.communityId}" value="Remove member"/>
                    </td>
                </c:if>
            </c:if>
        </tr>
    </c:forEach>
</table>
</body>
</html>

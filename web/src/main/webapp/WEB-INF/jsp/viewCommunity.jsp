<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--
  Created by IntelliJ IDEA.
  User: gyavr
  Date: 31.05.2021
  Time: 15:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Community</title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static-resources/style.css"/>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</head>
<body>
<jsp:include page="included.jsp"/>
<c:if test="${isMember && !isCreator}">
    <p><strong>You're a member!</strong></p>
    <input type="button" class="deleteMember" userid="${userID}" communityid="${community.id}" value="Leave community">
</c:if>
<div id="accordion">
    <h3>Information</h3>
    <div>
        <p>
        <p>${community.name}</p>
        <p>${community.description}</p>
        <p><img src="${pageContext.request.contextPath}/community/photo/${community.id}"/></p>
        </p>
    </div>
    <h3>Member list</h3>
    <div>
        <p>
            <jsp:include page="/community/membersList?communityId=${community.id}"/>
        </p>
    </div>
</div>
<c:if test="${!isMember}">
    <input type="button" class="addMember" userId="${userID}" communityId="${community.id}" value="Join community">
</c:if>
<c:if test="${isCreator}">
    <input type="button" value="Delete community" id="${community.id}" class="deleteCommunity"/>
    <p><a href="${pageContext.request.contextPath}/community/communityPhoto?communityId=${community.id}">Set community
        photo</a></p>
</c:if>
<script src="../../static-resources/const.js"></script>
<script src="../../static-resources/scripts.js"></script>
</body>
</html>

<%--
  Created by IntelliJ IDEA.
  User: gyavr
  Date: 08.06.2021
  Time: 15:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Photo editing</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static-resources/style.css"/>
</head>
<body>
<jsp:include page="included.jsp"/>
<p>
<form action="${pageContext.request.contextPath}/account/photo/${userID}" enctype="multipart/form-data"
      method="post">
    <strong>Upload a new one</strong>
    <table>
        <tr>
            <td><input type="file" name="photo"></td>
            <td><input type="submit" value="submit"></td>
        </tr>
    </table>
</form>
</p>
</body>
</html>

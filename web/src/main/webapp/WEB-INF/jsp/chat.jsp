<%--
  Created by IntelliJ IDEA.
  User: gyavr
  Date: 11.07.2021
  Time: 16:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Chat</title>
    <!-- https://cdnjs.com/libraries/sockjs-client -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sockjs-client/1.1.4/sockjs.min.js"></script>
    <!-- https://cdnjs.com/libraries/stomp.js/ -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/stomp.js/2.3.3/stomp.min.js"></script>
</head>
<body>
<jsp:include page="included.jsp"/>
<p><input type="button" id="disconnect" value="Disconnect"/></p>
<div id="chat-container">
    <div class="chat-header">
        <div class="user-container">
            <input id="username" type="hidden" value="${firstName}"/>
        </div>
        <h3>Chat</h3>
    </div>
    <hr/>
    <div id="connecting">Connected</div>
    <ul id="messageArea"></ul>
    <form id="messageForm" name="messageForm">
        <div class="input-message">
            <input type="text" id="message" autocomplete="off" placeholder="Type a message..."/>
            <input type="submit" value="Send">
        </div>
    </form>
</div>
<input id="chatId" type="hidden" value="${chatId}"/>
<script src="../../static-resources/chat-scripts.js"></script>
</body>
</html>

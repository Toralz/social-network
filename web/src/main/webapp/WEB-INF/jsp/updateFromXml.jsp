<%--
  Created by IntelliJ IDEA.
  User: gyavr
  Date: 03.07.2021
  Time: 13:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Update account from XML file</title>
</head>
<body>
<jsp:include page="included.jsp"/>
<p>
<form action="${pageContext.request.contextPath}/updateFromXml" enctype="multipart/form-data" method="post">
    <input type="file" name="xml"/>
    <input type="submit" value="Submit">
</form>
</p>
</body>
</html>

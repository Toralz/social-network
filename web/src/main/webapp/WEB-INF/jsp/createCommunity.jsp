<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: gyavr
  Date: 15.06.2021
  Time: 17:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static-resources/style.css"/>
    <title>Community creation</title>
</head>
<body>
<jsp:include page="included.jsp"/>
<p>
    <%--@elvariable id="community" type="com.getjavajob.training.siavrisi.socialnetwork.domain.CommunityDTO"--%>
    <form:form modelAttribute="community" action="${pageContext.request.contextPath}/community/createCommunity"
               method="post">
<table style width="50%">
    <tr>
        <td><form:label path="name">Name<strong style = "color:red">*</strong></form:label></td>
        <td><form:input path="name" type="text" name="name"/></td>
    </tr>
    <tr>
        <td><form:label path="description">Description<strong style = "color:red">*</strong></form:label></td>
        <td><form:input path="description" type="text" name="description"/></td>
    </tr>
</table>
<input type="submit" value="Submit">
</form:form>
</p>
</body>
</html>

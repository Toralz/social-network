<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: gyavr
  Date: 15.06.2021
  Time: 19:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static-resources/style.css"/>
    <title>Upload community photo</title>
</head>
<body>
<jsp:include page="welcome"/>
<jsp:include page="included.jsp"/>
<c:if test="${isCreator}">
    <p>
    <form action="${pageContext.request.contextPath}/community/photo/${community.id}" enctype="multipart/form-data"
          method="post">
        <strong>Upload a new one</strong>
        <table>
            <tr>
                <td><input type="file" name="photo"></td>
                <td><input type="submit" value="submit"></td>
                <input type="hidden" value="${community.id}" name="communityId"/>
            </tr>
        </table>
    </form>
    </p>
</c:if>
<c:if test="${!isCreator}">
    <strong style="color:red">Sorry, you're not an admin</strong>
</c:if>
</body>
</html>

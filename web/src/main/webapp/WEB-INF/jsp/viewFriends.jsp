<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: gyavr
  Date: 05.07.2021
  Time: 17:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Friends</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static-resources/style.css"/>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
            crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</head>
<body>
<jsp:include page="included.jsp"/>
<div id="accordion">
    <h3>Unaccepted requests</h3>
    <div>
        <p>
        <table>
            <c:forEach var="account" items="${unacceptedFrom}">
                <tr>
                    <td>
                        <a href="${pageContext.request.contextPath}/account/${account.id}">${account.firstName}</a>
                    </td>
                    <td>
                        <a href="${pageContext.request.contextPath}/account/${account.id}">${account.lastName}</a>
                    </td>
                    <td>
                        <a href="${pageContext.request.contextPath}/account/${account.id}">${account.patronymic}</a>
                    </td>
                    <td><input type="button" class="deleteFriend" id="${account.id}"
                               value="Delete friend"/></td>
                </tr>
            </c:forEach>
        </table>
        </p>
    </div>
    <h3>Incoming requests</h3>
    <div>
        <p>
        <table>
            <c:forEach var="account" items="${unacceptedTo}">
                <tr>
                    <td>
                        <a href="${pageContext.request.contextPath}/account/${account.id}">${account.firstName}</a>
                    </td>
                    <td>
                        <a href="${pageContext.request.contextPath}/account/${account.id}">${account.lastName}</a>
                    </td>
                    <td>
                        <a href="${pageContext.request.contextPath}/account/${account.id}">${account.patronymic}</a>
                    </td>
                    <td><input type="button" class="acceptFriend" id="${account.id}"
                               value="Accept friend"/></td>
                </tr>
            </c:forEach>
        </table>
        </p>
    </div>
    <h3>Accepted requests</h3>
    <div>
        <p>
        <table>
            <c:forEach var="account" items="${accepted}">
                <tr>
                    <td>
                        <a href="${pageContext.request.contextPath}/account/${account.id}">${account.firstName}</a>
                    </td>
                    <td>
                        <a href="${pageContext.request.contextPath}/account/${account.id}">${account.lastName}</a>
                    </td>
                    <td>
                        <a href="${pageContext.request.contextPath}/account/${account.id}">${account.patronymic}</a>
                    </td>
                    <td><input type="button" class="deleteFriend" id="${account.id}"
                               value="Delete friend"/></td>
                </tr>
            </c:forEach>
        </table>
        </p>
    </div>
</div>
<script src="../../static-resources/const.js"></script>
<script src="../../static-resources/scripts.js"></script>
<script src="../../static-resources/friend-scripts.js"></script>
</body>
</html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--
  Created by IntelliJ IDEA.
  User: gyavr
  Date: 29.06.2021
  Time: 17:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static-resources/style.css"/>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
            crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="../../static-resources/scripts.js"></script>
</head>
<body>
<jsp:include page="/account/welcome"/>
<a href="<c:url value="/index"/>" class="btn btn-primary" role="button">Home</a>
<a href="<c:url value="/searchAjax"/>" class="btn btn-primary" role="button">Search</a>
<a href="<c:url value="/account/editAccount"/>" class="btn btn-primary" role="button">Edit account</a>
<a href="<c:url value="/account/uploadPhoto"/>" class="btn btn-primary" role="button">Upload photo</a>
<a href="<c:url value="/community/communityCreation"/>" class="btn btn-primary" role="button">Create community</a>
<a href="<c:url value="/account/getXml"/>" class="btn btn-primary" role="button">Download XML</a>
<a href="<c:url value="/account/updateFromXml"/>" class="btn btn-primary" role="button">Upload XML</a>
<a href="<c:url value="/account/friends"/>" class="btn btn-primary" role="button">Friends</a>
<a href="<c:url value="/account/logout"/>" class="btn btn-primary" role="button">Logout</a>
</body>
</html>

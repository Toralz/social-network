let messageForm = document.getElementById('messageForm');
let messageInput = document.getElementById("message");
let messageArea = document.getElementById("messageArea");
let connectingElement = document.getElementById("connecting");
let chatId = $("#chatId").val();
let stompClient = null;
let username = null;

function connect() {
    username = $("#username").val();
    let socket = new SockJS("/ws");
    stompClient = Stomp.over(socket);
    stompClient.connect({}, onConnected, onError);
}

connect();

function onConnected() {
    stompClient.subscribe("/topic/" + chatId, onMessageReceived);
    stompClient.send("/app/chat/addUser/" + chatId, {}, JSON.stringify({sender: username, content: "connected"}));
    connectingElement.classList.add("hidden");
}

function onError(error) {
    connectingElement.textContent = "Could not connect to server.";
    connectingElement.style.color = "red";
}

function sendMessage(event) {
    let messageContent = messageInput.value.trim();
    if (messageContent && stompClient) {
        let chatMessage = {
            sender: username,
            content: " " + messageInput.value
        };
        stompClient.send("/app/chat/sendMessage/" + chatId, {}, JSON.stringify(chatMessage));
        messageInput.value = "";
    }
    event.preventDefault();
}

function onMessageReceived(payload) {
    let message = JSON.parse(payload.body);
    let messageElement = document.createElement("li");
    messageElement.classList.add("chat-message");
    let usernameElement = document.createElement("strong");
    usernameElement.classList.add("nickname");
    let usernameText = document.createTextNode(message.sender);
    usernameElement.appendChild(usernameText);
    messageElement.appendChild(usernameElement);
    let textElement = document.createElement("span");
    let messageText = document.createTextNode(message.content);
    textElement.appendChild(messageText);
    messageElement.appendChild(textElement);
    messageArea.appendChild(messageElement);
    messageArea.scrollTop = messageArea.scrollHeight;
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    console.log('Disconnected')
}

messageForm.addEventListener("submit", sendMessage, true);

$(document).ready(function () {
    $('#disconnect').click(function () {
        disconnect();
    });
});


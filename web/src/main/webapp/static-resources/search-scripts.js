let resultArray;
let item;

function displayAccounts() {
    $.map(resultArray, function (account, i) {
        let table = document.getElementById('accounts');
        let row = table.insertRow();
        let a = document.createElement('a');
        let text = document.createTextNode(account.firstName);
        a.append(text);
        a.href = URL + "account/" + account.id;
        let cell = row.insertCell();
        cell.append(a);
        cell = row.insertCell();
        a = document.createElement('a');
        text = document.createTextNode(account.lastName);
        a.append(text);
        a.href = URL + "account/" + account.id;
        cell.append(a);
        cell = row.insertCell();
        a = document.createElement('a');
        text = document.createTextNode(account.patronymic);
        a.append(text);
        a.href = URL + "account/" + account.id;
        cell.append(a);
    });
}

function displayCommunities() {
    $.map(resultArray, function (community, i) {
        let table = document.getElementById('communities');
        let row = table.insertRow();
        let a = document.createElement('a');
        let text = document.createTextNode(community.name);
        a.append(text);
        a.href = URL + "community/" + community.id;
        let cell = row.insertCell();
        cell.append(a);
        cell = row.insertCell();
        a = document.createElement('a');
        text = document.createTextNode(community.description);
        a.append(text);
        a.href = URL + "community/" + community.id;
        cell.append(a);
    });
}

function displayResult() {
    $('#accounts tr').remove();
    $('#communities tr').remove();
    if (item === '1') {
        displayAccounts();
    } else if (item === '2') {
        displayCommunities();
    }
}

$(document).ready(function () {
    let filter;
    $('#submitSearch').click(function () {
        $('#accounts tr').remove();
        $('#communities tr').remove();
        document.getElementById("pages").innerHTML = "";
        filter = $('#searchInput').val();
        item = $('#item').val();
        $.get(URL + "search?searchInput=" + filter + "&item=" + item + "&page=" + 1, function (data) {
            resultArray = data;
            displayResult();
            $.get(URL + "searchCount?searchInput=" + filter + "&item=" + item, function (data) {
                let i = 1;
                $('<input />', {type: 'submit', id: 'page', value: i}).appendTo('#pages');
                while (i * 5 < data) {
                    i++;
                    $('<input />', {type: 'submit', id: 'page', value: i}).appendTo('#pages');
                }
            });
        });
    });
    $('#pages').on('click', '#page', function () {
        $.get(URL + "search?searchInput=" + filter + "&item=" + item + "&page=" + this.value, function (data) {
            resultArray = data;
            displayResult(this.val);
        });
    });
});
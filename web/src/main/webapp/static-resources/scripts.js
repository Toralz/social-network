$(function () {
    $("#accordion").accordion();
});

$(function () {
    $("#datepicker").datepicker();
});

function validatePhone() {
    let equality = false;
    let phone = document.getElementById("newPhone").value;
    if (phone.match(/^((\+7|7|8)+([0-9]){10})$/gm)) {
        $("#phones tr").each(function () {
            if ($(this).find("td:first").html().localeCompare(phone) === 0) {
                equality = true;
            }
        });
        if (equality) {
            alert("Phone should be unique")
            return false;
        }
        addPhone(phone);
        return true;
    } else {
        alert("Wrong phone number format");
        return false;
    }
}

function addPhone(phone) {
    let table = document.getElementById("phones");
    let row = table.insertRow();
    let cell = row.insertCell();
    cell.id = phone;
    cell.innerHTML = phone;
    cell = row.insertCell(1);
    $('<input/>', {type: 'hidden', value: phone, name: 'newPhones'}).appendTo(cell);
    $('<button/>', {text: 'Delete phone', onclick: 'deletePhone(this)'}).appendTo(cell);
}

function deletePhone(r) {
    let table = document.getElementById("phones");
    let number = r.parentNode.parentNode.getElementsByTagName('td')[0].innerHTML;
    $('<input/>', {type: 'hidden', value: number, name: 'removedPhones'}).appendTo('#removedPhones')
    let index = r.parentNode.parentNode.rowIndex;
    table.deleteRow(index);
}

$(document).ready(function () {
    let communityId = parseInt($(this).attr("communityid"));
    $('.deleteCommunity').click(function () {
        $.ajax(URL + "community/" + communityId, {
            method: "DELETE",
            error: function () {
                window.location.href = URL + "index";
            },
            success: function () {
                window.location.href = URL + "index";
            }
        });
    });
    $('.addMember').click(function () {
        let communityId = parseInt($(this).attr("communityid"));
        $.ajax(URL + "community/member/" + communityId + "/" + parseInt($(this).attr("userid")), {
            method: "POST",
            error: function () {
                location.reload();
            },
            success: function () {
                window.location.href = URL + "index";
            }
        });
    });
    $('.deleteMember').click(function () {
        let communityId = parseInt($(this).attr("communityid"));
        $.ajax(URL + "community/member/" + communityId + "/" + parseInt($(this).attr("userid")), {
            method: "DELETE",
            error: function () {
                location.reload();
            },
            success: function () {
                window.location.href = URL + "index";
            }
        });
    });
});

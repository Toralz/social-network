$(document).ready(function () {
    $('.addFriend').click(function () {
        let friendId = $(this).attr("id");
        $.post(URL + "account/friends/" + parseInt(friendId), function (data) {
            $('.addFriend').remove();
        });
    });
    $('.deleteFriend').click(function () {
        let friendId = $(this).attr("id");
        $.ajax(URL + "account/friends/" + parseInt(friendId), {
            method: "DELETE",
            success: function () {
                location.reload();
            },
            error: function () {
                location.reload();
            }
        });
    });
    $('.acceptFriend').click(function () {
        let friendId = $(this).attr("id");
        $.ajax(URL + "account/friends/" + parseInt(friendId), {
            method: "PUT",
            success: function () {
                location.reload();
            },
            error: function () {
                location.reload();
            }
        });
    });
});
package com.getjavajob.training.siavrisi.socialnetwork.repository;

import com.getjavajob.training.siavrisi.socialnetwork.config.TestDaoConfiguration;
import com.getjavajob.training.siavrisi.socialnetwork.dao.AccountDao;
import com.getjavajob.training.siavrisi.socialnetwork.dao.FriendshipDao;
import com.getjavajob.training.siavrisi.socialnetwork.domain.Account;
import com.getjavajob.training.siavrisi.socialnetwork.domain.Friendship;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Transactional
@Rollback(false)
@ContextConfiguration(classes = TestDaoConfiguration.class)
public class FriendshipRepositoryTest {

    private final AccountDao accountRepository;

    private final FriendshipDao friendshipRepository;

    @Autowired
    public FriendshipRepositoryTest(AccountDao accountRepository, FriendshipDao friendshipRepository) {
        this.accountRepository = accountRepository;
        this.friendshipRepository = friendshipRepository;
    }

    private void setUp() {
        Account[] accounts = new Account[5];
        for (int i = 0; i < 5; i++) {
            accounts[i] = new Account();
            accounts[i].setFirstName(String.valueOf(i));
            accounts[i].setLastName(String.valueOf(i));
            accounts[i].setPatronymic(String.valueOf(i));
            accounts[i].setPassword(String.valueOf(i));
            accounts[i].setBirthday(new Date(System.currentTimeMillis()));
            accountRepository.save(accounts[i]);
        }

    }

    @Test
    @Order(1)
    void addFriend() {
        setUp();
        Friendship friendship = new Friendship();
        friendship.setKey(new Friendship.Key(1, 2));
        friendship.setAccountFrom(accountRepository.get(1));
        friendship.setAccountTo(accountRepository.get(2));
        friendship.setAccepted(false);
        friendshipRepository.insert(friendship);
        friendship = new Friendship();
        friendship.setKey(new Friendship.Key(3, 2));
        friendship.setAccountFrom(accountRepository.get(3));
        friendship.setAccountTo(accountRepository.get(2));
        friendship.setAccepted(false);
        friendshipRepository.insert(friendship);
        assertEquals(1, friendshipRepository.getByAccountFrom(1).size());
        assertEquals(2, friendshipRepository.getByAccountTo(2).size());
    }

    @Test
    @Order(2)
    void deleteFriend() {
        friendshipRepository.delete(new Friendship.Key(3, 2));
        assertEquals(1, friendshipRepository.getByAccountTo(2).size());
    }

    @Test
    @Order(3)
    void acceptFriend() {
        Friendship friendship = new Friendship();
        friendship.setKey(new Friendship.Key(4, 2));
        friendship.setAccountFrom(accountRepository.get(4));
        friendship.setAccountTo(accountRepository.get(2));
        friendship.setAccepted(false);
        friendshipRepository.insert(friendship);
        friendship = new Friendship();
        friendship.setKey(new Friendship.Key(5, 2));
        friendship.setAccountFrom(accountRepository.get(5));
        friendship.setAccountTo(accountRepository.get(2));
        friendship.setAccepted(false);
        friendshipRepository.insert(friendship);
        friendship = friendshipRepository.get(new Friendship.Key(4, 2));
        friendship.setAccepted(true);
        friendshipRepository.insert(friendship);
        assertEquals(3, friendshipRepository.getByAccountTo(2).size());
        assertEquals(1, friendshipRepository.getByAccountToAccepted(2, true).size());
    }

}

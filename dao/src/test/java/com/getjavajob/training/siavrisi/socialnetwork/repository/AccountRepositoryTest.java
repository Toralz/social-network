package com.getjavajob.training.siavrisi.socialnetwork.repository;

import com.getjavajob.training.siavrisi.socialnetwork.config.TestDaoConfiguration;
import com.getjavajob.training.siavrisi.socialnetwork.dao.AccountDao;
import com.getjavajob.training.siavrisi.socialnetwork.domain.Account;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Transactional
@Rollback(false)
@ContextConfiguration(classes = TestDaoConfiguration.class)
class AccountRepositoryTest {

    private final AccountDao accountRepository;

    @Autowired
    public AccountRepositoryTest(AccountDao accountRepository) {
        this.accountRepository = accountRepository;
    }

    public void createAccounts() {
        for (int i = 0; i < 7; i++) {
            Account account = new Account();
            account.setFirstName("1");
            account.setLastName("1");
            account.setPatronymic("1");
            account.setPassword("1");
            account.setEmail(String.valueOf(i));
            account.setBirthday(new Date(System.currentTimeMillis()));
            accountRepository.save(account);
        }
        Account account = new Account();
        account.setFirstName("5");
        account.setLastName("5");
        account.setEmail("test");
        account.setPassword("5");
        account.setBirthday(new Date(System.currentTimeMillis()));
        accountRepository.save(account);
    }

    @Order(1)
    @Test
    void getByName() {
        createAccounts();
        Account account = accountRepository.getByEmail("test");
        assertEquals("5", account.getFirstName());
    }

    @Order(2)
    @Test
    void getLikePaginate() {
        List<Account> accounts =
                accountRepository.getLike("1", 1);
        assertEquals(2, accounts.size());
        assertEquals(7, accountRepository.getCount("1"));
    }

}
package com.getjavajob.training.siavrisi.socialnetwork.repository;

import com.getjavajob.training.siavrisi.socialnetwork.config.TestDaoConfiguration;
import com.getjavajob.training.siavrisi.socialnetwork.dao.CommunityDao;
import com.getjavajob.training.siavrisi.socialnetwork.domain.Community;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Transactional
@Rollback(false)
@ContextConfiguration(classes = TestDaoConfiguration.class)
class CommunityRepositoryTest {

    private final CommunityDao communityRepository;

    @Autowired
    public CommunityRepositoryTest(CommunityDao communityRepository) {
        this.communityRepository = communityRepository;
    }

    public void createCommunities() {
        for (int i = 0; i < 8; i++) {
            Community community = new Community();
            community.setName(String.valueOf(i));
            community.setDescription("1");
            communityRepository.save(community);
        }
        Community community = new Community();
        community.setName("test");
        community.setDescription("testDescription");
        communityRepository.save(community);
    }

    @Test
    @Order(1)
    void getByName() {
        createCommunities();
        Community community = communityRepository.getByName("test");
        assertEquals("testDescription", community.getDescription());
    }

    @Test
    @Order(2)
    void getLikePaginate() {
        List<Community> communities = communityRepository.getLike("1",
                1);
        assertEquals(8, communityRepository.getCount("1"));
        assertEquals(3, communities.size());
    }

}
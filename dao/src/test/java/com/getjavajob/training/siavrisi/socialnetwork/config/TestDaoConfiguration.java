package com.getjavajob.training.siavrisi.socialnetwork.config;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.io.IOException;
import java.util.Properties;

@TestConfiguration
@EnableJpaRepositories(basePackages = "com.getjavajob.training.siavrisi.socialnetwork.repository")
@EnableTransactionManagement(proxyTargetClass = true)
public class TestDaoConfiguration {

    @Bean
    public BasicDataSource dataSource() {
        BasicDataSource dataSource = new BasicDataSource();
        Properties properties = new Properties();
        try {
            properties.load(TestDaoConfiguration.class.getClassLoader().getResourceAsStream("DBConfig.properties"));
            dataSource.setUrl(properties.getProperty("database.h2"));
            dataSource.setUsername(properties.getProperty("database.user"));
            dataSource.setPassword(properties.getProperty("database.password"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return dataSource;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource());
        em.setPackagesToScan("com.getjavajob.training.siavrisi.socialnetwork.domain");
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        vendorAdapter.setDatabasePlatform("org.hibernate.dialect.MySQLDialect");
        vendorAdapter.setShowSql(true);
        em.setJpaVendorAdapter(vendorAdapter);
        return em;
    }

    @Bean
    public PlatformTransactionManager transactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
        return transactionManager;
    }

}

CREATE TABLE IF NOT EXISTS ACCOUNT
(
    `userID`      int         NOT NULL AUTO_INCREMENT,
    `firstName`   varchar(45) NOT NULL,
    `lastName`    varchar(45) NOT NULL,
    `patronymic`  varchar(45) DEFAULT NULL,
    `ICQ`         varchar(45) DEFAULT NULL,
    `email`       varchar(45) DEFAULT NULL,
    `skype`       varchar(45) DEFAULT NULL,
    `homeAddress` varchar(45) DEFAULT NULL,
    `workAddress` varchar(45) DEFAULT NULL,
    `extra`       varchar(45) DEFAULT NULL,
    `password`    varchar(90) DEFAULT NULL,
    `photo`       BLOB,
    `birthday`    date        NOT NULL,
    PRIMARY KEY (`userID`),
    UNIQUE KEY `userID_UNIQUE` (`userID`),
    UNIQUE KEY `email_UNIQUE` (`email`),
    UNIQUE KEY `skype_UNIQUE` (`skype`),
    UNIQUE KEY `ICQ_UNIQUE` (`ICQ`)
);
CREATE TABLE IF NOT EXISTS COMMUNITY
(
    `id`          int         NOT NULL AUTO_INCREMENT,
    `name`        varchar(45) NOT NULL,
    `description` varchar(90) NOT NULL,
    `photo`       BLOB,
    `creatorId`   int,
    PRIMARY KEY (`id`)
);
CREATE TABLE IF NOT EXISTS FRIEND
(
    `account1Id` int      NOT NULL,
    `account2Id` int      NOT NULL,
    `created`    datetime NOT NULL,
    PRIMARY KEY (`account1Id`, `account2Id`)
);
CREATE TABLE IF NOT EXISTS PHONE
(
    `accountID` int DEFAULT NULL,
    `phone`     varchar(45) NOT NULL,
    PRIMARY KEY (`phone`),
    UNIQUE KEY `phone_UNIQUE` (`phone`),
    CONSTRAINT `fk_phone_account1` FOREIGN KEY (`accountID`) REFERENCES `account` (`userID`)
);
CREATE TABLE IF NOT EXISTS communitymembership
(
    `communityID` int NOT NULL,
    `userID`      int NOT NULL,
    PRIMARY KEY (`communityID`, `userID`),
    CONSTRAINT `fk_communityMembership_account1` FOREIGN KEY (`userID`) REFERENCES `account` (`userID`),
    CONSTRAINT `fk_communityMembership_community1` FOREIGN KEY (`communityID`) REFERENCES `community` (`id`)
);
CREATE TABLE IF NOT EXISTS `friendship`
(
    `friendshipId`       int(11) NOT NULL AUTO_INCREMENT,
    `accountTo_userID`   int(11) NOT NULL,
    `accountFrom_userID` int(11) NOT NULL,
    `accepted`           tinyint(4) DEFAULT '0',
    PRIMARY KEY (`friendshipId`, `accountTo_userID`, `accountFrom_userID`),
    CONSTRAINT `fk_friendship_account1` FOREIGN KEY (`accountFrom_userID`) REFERENCES `account` (`userID`),
    CONSTRAINT `fk_friendship_account2` FOREIGN KEY (`accountTo_userID`) REFERENCES `account` (`userID`)
)


package com.getjavajob.training.siavrisi.socialnetwork.dao;

import java.util.List;

public interface Dao<T> {

    T get(int id);

    List<T> getAll();

    T save(T t);

    void delete(int id);

    T update(int id, T t);

}

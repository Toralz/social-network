package com.getjavajob.training.siavrisi.socialnetwork.dao;

import com.getjavajob.training.siavrisi.socialnetwork.domain.Account;

import java.util.List;

public interface AccountDao extends Dao<Account> {

    List<Account> getLike(String part, int page);

    int getCount(String part);

    Account getByEmail(String email);

}

package com.getjavajob.training.siavrisi.socialnetwork.repository;

import com.getjavajob.training.siavrisi.socialnetwork.dao.FriendshipDao;
import com.getjavajob.training.siavrisi.socialnetwork.domain.Friendship;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FriendshipRepository extends CrudRepository<Friendship, Friendship.Key>, FriendshipDao {

    /**
     * @param accountFromId account to get requests from
     * @return list of friendship requests going from <code>accountFromId</code>
     */
    List<Friendship> findByKeyAccountFromId(int accountFromId);


    /**
     * @param accountToId account to get requests to
     * @return list of friendship requests going to <code>accountToId</code>
     */
    List<Friendship> findByKeyAccountToId(int accountToId);

    /**
     * @param accountToId account to get requests to
     * @param accepted    filter to get only accepted/unaccepted requests
     * @return list of friendship requests going to <code>accountToId</code> with request status equals
     * <code>accepted</code>
     */
    List<Friendship> findByKeyAccountToIdAndAcceptedEquals(int accountToId, boolean accepted);

    /**
     * @param accountFromId account to get requests from
     * @param accepted      filter to get only accepted/unaccepted requests
     * @return list of friendship requests going from <code>accountFromId</code> with request status equals
     * <code>accepted</code>
     */
    List<Friendship> findByKeyAccountFromIdAndAcceptedEquals(int accountFromId, boolean accepted);

    @Override
    default List<Friendship> getByAccountFrom(int id) {
        return findByKeyAccountFromId(id);
    }

    @Override
    default List<Friendship> getByAccountTo(int id) {
        return findByKeyAccountToId(id);
    }

    @Override
    default List<Friendship> getByAccountFromAccepted(int id, boolean accepted) {
        return findByKeyAccountFromIdAndAcceptedEquals(id, accepted);
    }

    @Override
    default List<Friendship> getByAccountToAccepted(int id, boolean accepted) {
        return findByKeyAccountToIdAndAcceptedEquals(id, accepted);
    }

    @Override
    default void delete(Friendship.Key key) {
        deleteById(key);
    }

    @Override
    default Friendship update(Friendship.Key key, Friendship friendship) {
        friendship.setKey(key);
        return save(friendship);
    }

    @Override
    default Friendship get(Friendship.Key key) {
        return findById(key).orElse(new Friendship());
    }

    @Override
    default Friendship insert(Friendship friendship) {
        return save(friendship);
    }

}

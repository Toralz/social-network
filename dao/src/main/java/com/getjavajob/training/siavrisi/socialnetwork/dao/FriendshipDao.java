package com.getjavajob.training.siavrisi.socialnetwork.dao;

import com.getjavajob.training.siavrisi.socialnetwork.domain.Friendship;

import java.util.List;

public interface FriendshipDao {

    void delete(Friendship.Key key);

    Friendship update(Friendship.Key key, Friendship friendship);

    Friendship insert(Friendship friendship);

    Friendship get(Friendship.Key key);

    List<Friendship> getByAccountFrom(int id);

    List<Friendship> getByAccountTo(int id);

    List<Friendship> getByAccountFromAccepted(int id, boolean accepted);

    List<Friendship> getByAccountToAccepted(int id, boolean accepted);

}

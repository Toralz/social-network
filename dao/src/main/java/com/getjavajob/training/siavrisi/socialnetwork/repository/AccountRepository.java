package com.getjavajob.training.siavrisi.socialnetwork.repository;

import com.getjavajob.training.siavrisi.socialnetwork.dao.AccountDao;
import com.getjavajob.training.siavrisi.socialnetwork.domain.Account;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AccountRepository extends JpaRepository<Account, Integer>, AccountDao {

    /**
     * @param email to search account with
     * @return returns account containing specified email
     */
    Account getByEmailEquals(String email);

    /**
     * @param firstName  part to be searched like
     * @param lastName   part to be searched like
     * @param patronymic part to be searched like
     * @param pageable   page results displayed at
     * @return list of accounts searched like given part on specified page
     */
    List<Account> getByFirstNameContainingOrLastNameContainingOrPatronymicContaining(String firstName,
                                                                                     String lastName,
                                                                                     String patronymic,
                                                                                     Pageable pageable);

    @Override
    default Account update(int id, Account account) {
        account.setId(id);
        return save(account);
    }

    @Override
    default List<Account> getLike(String part, int page) {
        return getByFirstNameContainingOrLastNameContainingOrPatronymicContaining(part, part, part,
                PageRequest.of(page, 5));
    }

    @Override
    default int getCount(String part) {
        return countByFirstNameContainingOrLastNameContainingOrPatronymicContaining(part, part, part);
    }

    @Override
    default void delete(int id) {
        deleteById(id);
    }

    @Override
    default List<Account> getAll() {
        return findAll();
    }

    @Override
    default Account get(int id) {
        return findById(id).orElse(new Account());
    }

    /**
     * @param firstName  part to be searched like
     * @param lastName   part to be searched like
     * @param patronymic part to be searched like
     * @return how many results are found by given part
     */
    int countByFirstNameContainingOrLastNameContainingOrPatronymicContaining(String firstName, String lastName,
                                                                             String patronymic);

}

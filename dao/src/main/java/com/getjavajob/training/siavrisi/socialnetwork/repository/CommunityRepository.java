package com.getjavajob.training.siavrisi.socialnetwork.repository;

import com.getjavajob.training.siavrisi.socialnetwork.dao.CommunityDao;
import com.getjavajob.training.siavrisi.socialnetwork.domain.Community;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CommunityRepository extends JpaRepository<Community, Integer>, CommunityDao {

    /**
     * @param name to search account with
     * @return community with specified name
     */
    Community getByNameEquals(String name);

    /**
     * @param name        part to be searched like
     * @param description part to be searched like
     * @param pageable    page results displayed at
     * @return list of communities searched like given part on specified page
     */
    List<Community> getByNameContainingOrDescriptionContaining(String name, String description,
                                                               Pageable pageable);

    /**
     * @param name        part to be searched like
     * @param description part to be searched like
     * @return how many results are found by given part
     */
    int countByNameContainingOrDescriptionContaining(String name, String description);

    @Override
    default Community getByName(String name) {
        return getByNameEquals(name);
    }

    @Override
    default List<Community> getAll() {
        return findAll();
    }

    @Override
    default Community get(int id) {
        return findById(id).orElse(new Community());
    }

    @Override
    default void delete(int id) {
        deleteById(id);
    }

    @Override
    default List<Community> getLike(String part, int page) {
        return getByNameContainingOrDescriptionContaining(part, part, PageRequest.of(page, 5));
    }

    @Override
    default int getCount(String part) {
        return countByNameContainingOrDescriptionContaining(part, part);
    }

    @Override
    default Community update(int id, Community community) {
        community.setId(id);
        save(community);
        return community;
    }

}

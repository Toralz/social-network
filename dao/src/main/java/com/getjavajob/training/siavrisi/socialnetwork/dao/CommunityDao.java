package com.getjavajob.training.siavrisi.socialnetwork.dao;

import com.getjavajob.training.siavrisi.socialnetwork.domain.Community;

import java.util.List;

public interface CommunityDao extends Dao<Community> {

    List<Community> getLike(String part, int page);

    int getCount(String part);

    Community getByName(String name);

}
